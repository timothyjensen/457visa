<?php
/**
 * Template Name: Full Flowing
 *
 */ 
get_header(); ?>

        <div id="inner_content_area"><!--start content_area-->
            <div id="about_content_wrapper">
                <div class="top_about_box"><div class="btm_about_box"><div class="about_box_bg">
                    <div class="about_con">
                    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="about_con_text">
                            <img src="<?php bloginfo('template_url') ?>/images/about_top_pic.png" alt="" class="about_top_bg" />
                            <?php echo wpautop(get_post_meta( get_the_ID(), 'top_img_text', true )); ?>
                        </div>
                        <div class="about_con_text" style="width: 100%">
                            <?php the_content(); ?>
                        </div>
                         <?php endwhile; endif; ?> 
                    </div>
                    <div class="about_feature_area">
                    <div class="block_aboutbtmcontent">
                    	<?php the_block('aboutbtmcontent'); ?> 
                    </div>
                    <?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
                    </div>
                </div></div></div>
            </div>    
        </div><!--//end #content_area-->
                  
                    
<?php get_footer(); ?>