<?php
/**
 * Template Name: Ratings template
 *
 */ 


if(isset($_POST['catID']) && isset($_POST['catID2'])){
	switch($_POST['catID']){
		case 1:
			if($_POST['catID2'] == 1) $page_red_id = 109;
			if($_POST['catID2'] == 2) $page_red_id = 216;
			if($_POST['catID2'] == 3) $page_red_id = 214;
			break;
		case 2:
			if($_POST['catID2'] == 1) $page_red_id = 203;
			if($_POST['catID2'] == 2) $page_red_id = 195;
			if($_POST['catID2'] == 3) $page_red_id = 183;
			break;
		case 3:
			if($_POST['catID2'] == 1) $page_red_id = 211;
			if($_POST['catID2'] == 2) $page_red_id = 208;
			if($_POST['catID2'] == 3) $page_red_id = 205;
			break;
	}
	header("Location: " . get_permalink($page_red_id));
	die('');
}

$insurance_wrap = "";
$mypageID = $post->ID;
switch($post->ID){
	case 109:
		$_GET['catID'] = 1; $_GET['catID2'] = 1; break;
	case 216:
		$_GET['catID'] = 1; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; break;
	case 214:
		$_GET['catID'] = 1; $_GET['catID2'] = 3; break;
	case 203:
		$_GET['catID'] = 2; $_GET['catID2'] = 1; break;
	case 195:
		$_GET['catID'] = 2; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; break;
	case 183:
		$_GET['catID'] = 2; $_GET['catID2'] = 3; break;
	case 211:
		$_GET['catID'] = 3; $_GET['catID2'] = 1; break;
	case 208:
		$_GET['catID'] = 3; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; break;
	case 205:
		$_GET['catID'] = 3; $_GET['catID2'] = 3; break;
}

get_header(); ?>

        <div id="inner_content_area"><!--start content_area-->
        	<div id="inner-wrapper"><!--start inner-wrapper-->
                <div class="insurance-topcontent"><!--start insurance-topcontent-->
                    <div class="text-content">
                    
            	<?php
            		$imgtitle = trim(get_post_meta($post->ID, 'top_img_text', true));
            		$bg_img_url = trim(get_post_meta($post->ID, 'bg_img_url', true)); 
            		if($bg_img_url != '') {
            			$bg_img_url = "background: url('" . $bg_img_url . "') no-repeat scroll 0% 0% transparent";
            			$bg_img_url = 'style="' . $bg_img_url . '"';
            		}
            		 
            		if($imgtitle == "") $imgtitle = "457 visa health insurance";
	            	if($mypageID == 1703){
            			 if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Health Insurance German Table Widget') );
            			 query_posts(array('post_type' => 'page','post__in' => array(109)));
					} else if($mypageID == 1719){
						$imgtitle = "485 visa health insurance";
						$imgstyle = "width:200px;height:150px";
            			 if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('485 Health Insurance Table Widget') );
            		 } else {
            	?>
                    <?php /* if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Health Insurance Table Widget') ) : ?><?php endif */?>
                    <?php echo trim(get_post_meta($post->ID, 'custom_title', true)); ?>
               	<?php } ?>
                    
                <?php if($mypageID != 1719){ ?>
                    <div class="selec-field">
                    	<form name="catSelect" action="" method="post" >
                    	<div class="rowElem rowElem1">
                            <label>I'm looking for cover:</label>
                            <select name="catID" id="catID" onchange="catSelect.submit()">
                                <option value="1"<?php if($_GET['catID'] == '1') echo ' selected="selected"' ?>>Just Myself</option>
                            
                                <option value="2"<?php if($_GET['catID'] == '2') echo ' selected="selected"' ?>>Me &amp; my partner</option>
                                <option value="3"<?php if($_GET['catID'] == '3') echo ' selected="selected"' ?>>My family</option>
                            </select>
                        </div>
                        <div class="rowElem">
                        <label>My Main Priority is:</label>
                            <select name="catID2" id="catID2" onchange="catSelect.submit()">
                                <option value="1"<?php if($_GET['catID2'] == '1') echo ' selected="selected"' ?>>To Meet 457 Visa Requirements</option>
                            
                                <option value="2"<?php if($_GET['catID2'] == '2') echo ' selected="selected"' ?>>457 Visa Cover With Some Extras</option>
                            
                                <option value="3"<?php if($_GET['catID2'] == '3') echo ' selected="selected"' ?>>To Have The Top Level Cover</option>
                                
                             </select>
                         </div>
                       </form>
                    </div>
                <?php } ?>
                </div>
                <?php // if(has_post_thumbnail()) { the_post_thumbnail( 'ratings_thumbnail' ) ;} ?>
                <img src="<?php bloginfo('template_url') ?>/images/image01.png" width="300" height="230" style="<?php echo $imgstyle; ?>" alt="<?php echo $imgtitle; ?>" title="<?php echo $imgtitle; ?>" />
                <div class="clear"></div>
            </div><!--//end #insurance-topcontent-->
        </div><!--//end #content_area-->
        <div class="clear"></div>
        
         <div id="insurance-outerwrap">
            <div id="insurance-wrap" class="<?php echo $insurance_wrap; ?>" <?php echo $bg_img_url; ?>>
                   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="insurancebox_content">
                   <?php the_content('More...'); ?>
                   <?php //$insurance_text = trim(get_post_meta($post->ID, '_insurance_text', true)); ?>
                   <?php $rating_text = trim(get_post_meta($post->ID, 'rating_text', true)); ?>
                   </div>
                   <?php endwhile;endif;
                   wp_reset_query();
                   ?>
            </div>
            <div class="clear" id="removed_fixed_arrow"></div>
            <div class="instructions">
            	<div class="instructions_inner">
            	<h4>This information is indicative and provided for general information only.<br />Please refer to the relevant policy brochure for full details including waiting
                 period.</h4>
                 <div class="instructions_one">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_tick.png" align="tick" /><span>This policy is a good match for this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_two">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_exclo.png" align="tick" /><span>This policy only covers some areas of this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_three">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_cross.png" align="tick" /><span>This policy does not offer comprehensive cover of this insurance item</span>
                 </div>
                 </div>
            </div>
            <div class="clear"></div>
                    
            <div class="insurance-btmcontent">
            		<div class="bankbox_bg"><div class="bankbox_topbg"><div class="bankbox_btmbg">
                        <div class="bank-entry">
                            <div class="block_ratingsbtmcontent">
                    			<?php the_block('ratingsbtmcontent'); ?>
                    			<?php echo $rating_text; ?>                    
                			</div>
							<?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
                        </div>
                    </div></div></div>
                </div>
       </div>
       </div>
                  
                    
<?php get_footer(); ?>