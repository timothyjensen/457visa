<?php
global $tips;
$tips = array();
$page_tem = get_page_template();


if(strpos($page_tem,"template-ratings.php") !== false){
/* Health Insurance Tips Starts */

	$tips[1] = "These policies meet Condition 8501, which requires all visa holders, including accompanying family members, to maintain adequate arrangements for health insurance for the duration of their stay in Australia.";
	
	$tips[2] = "Cover within a public hospital when treatment leads to an admission as In-patient, or is certified by the treating doctor as a genuine emergency"; 
	
	$tips[3] = "Private hospital accommodation (overnight and day-only, including theatre, intensive care, ward drugs. Exception- lower benefits apply for pregnancy, psychiatric, palliative and gastric banding. Please note that waiting periods apply and pre-existing conditions may not be covered)";
	
	$tips[4] = "Cover when in a hospital including any specialist/doctors who visit and treat you.";
	
	$tips[5] = "Pharmaceutical drugs that are medically necessary and listed under the Pharmaceutical Benefits Scheme (PBS)";
	
	$tips[6] = "Medical repatriation to home country when deemed medically necessary by a Medical Practitioner appointed by the fund.  Medical repatriation may include the cost of air fares, on-board stretcher, accompanying aero-medical specialists and nursing staff, and ambulance transport in your home country.";
	
	$tips[7] = "Many policies have Public hospital cover (100% MBS fee In-patient medical) but funds that have ticks provide private hospital cover for pregnancy & birth related services.";
	
	$tips[8] = "Ambulance service covered within Australia that is provided by a State or Territory Ambulance  and defined by the relevant service provider as emergency ambulance transport or where an ambulance is called to attend an emergency but on arriving is no longer required this charge will also be covered; or defined by a treating doctor as medically necessary transport";
	
	$tips[9] = "Benefits for extra services including general dental, optical, dietetics, home nursing, occupational therapy, podiatry, hearing aids, major dental.";
	
	$tips[10] = "Key additional benefits/limitations of each service.";
	
	$tips[11] = "Public Hospital Accommodation - 100% of the rates raised by the public hospital If you choose to be treated in a private hospital you'll receive minimum benefits only. This is equivalent to the Public Hospital benefit for admitted patients that is equal to the State and Territory gazetted rates for ineligible patients. This may result in large out-of-pocket expenses. In-patient Medical - 100% of the MBS fee for In-patient medical provided in a hospital including doctors, specialists, pathology and radiology. Pregnancy and birth related services are payable after 12 months Waiting Period has been served.";
	
/* Health Insurance Tips Ends */
}


if(strpos($page_tem,"template-agents.php") !== false){
/* Migration Agents Tips Starts */

	$tips[1] = "Are the member(s) of the migration service provider (MSP) lawyers (who hold a current legal practicing certificate) as well as migration agents?";
	
	$tips[2] = "Does the MSP's website show that they are experienced providing services to both companies and individuals?"; 
	
	$tips[3] = "Does the MSP advertise a free initial assessment?";
	
	$tips[4] = "Is the MSP based in a major central business district?";
	
	$tips[5] = "Does the migration agent/lawyer advertise that they offer other services? Eg. Employment law advice, assistance with relocation etc";
	
	$tips[6] = "Does the MSP employ more than one migration agent/lawyer agent?";
	
	$tips[7] = "Does the MSP publish easily accessible regular immigration law updates on their website?";
	
	$tips[8] = "Key membership/affiliation as shown on the agent's site?";
	
	$tips[9] = "Location of the agents main office";
	
/* Migration Agents Tips Ends */
}


if(strpos($page_tem,"template-travel.php") !== false){
/* Travel Insurance Tips Starts */

	$tips[1] = "Name of the cover given by the insurer";
	
	$tips[2] = "Company the policy is underwritten by"; 
	
	$tips[3] = "Cover for overseas medical and dental expenses";
	
	$tips[4] = "Cover for overseas medical and dental expenses";
	
	$tips[5] = "A 24/7 emergency phone number for help when overseas";
	
	$tips[6] = "Cover for personal items owned by you and that you take with you or buy on your journey";
	
	$tips[7] = "Cover for cancellation fees and lost deposits for pre-paid travel arrangements for unforeseen circumstances";
	
	$tips[8] = "The amount which an individual must first pay for each claim arising from the one event before a claim can be made under the policy";
	
/* Travel Insurance Tips Ends */
}

if(strpos($page_tem,"template-bank.php") !== false){
/* Bank Account Tips Starts */

	$tips[1] = "Monthly account keeping fees for maintaining an account with the bank. Additional charges may apply.";
	
	$tips[2] = "Debit card provided with the account. Credit cards can be applied for after opening the account"; 
	
	$tips[3] = "The ability to bank over the internet and view account information";
	
	$tips[4] = "The ability to bank using the phone (such as paying bills)";
	
	$tips[5] = "Other options offered";
	
/* Bank Account Tips Ends */
}

?>