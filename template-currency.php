<?php
/**
 * Template Name: Currency Exchane template
 *
 */ 
get_header(); ?>

<div id="inner_content_area"><!--start content_area-->
  <div id="inner-wrapper"><!--start inner-wrapper-->
    <div class="insurance-topcontent currencybg"><!--start insurance-topcontent-->
      <div class="text-content">
        <div class="block_currencytopcontent">
          <?php the_block('currencytopcontent'); ?>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <!--//end #insurance-topcontent--> 
  </div>
  <!--//end #content_area-->
  <div class="clear"></div>
  <div id="insurance-outerwrap">
    <div id="currency-wrap">
      <div class="insurancebox_content">
        <div class="block_currencyratings">
          <?php the_block('currencyratings'); ?>
        </div>
      </div>
    </div>
    <div class="bank-btmcontent">
      <div class="bankbox_bg">
        <div class="bankbox_topbg">
          <div class="bankbox_btmbg">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="bank-entry">
              <?php the_content('More...'); ?>
            </div>
                			<?php /*
                            <div class="sharethis"> <span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='facebook'></span><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='twitter'></span><span class='st_sharethis_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='sharethis'></span><span class='st_plusone_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='plusone'></span></div> 
                            */ 
                            show_social_media(get_permalink(), get_the_title());
                            ?>
            <?php endwhile;endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>