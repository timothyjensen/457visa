<?php



add_action('init','of_options');



if (!function_exists('of_options')) {

function of_options(){

	

//Access the WordPress Categories via an Array

$of_categories = array();  

$of_categories_obj = get_categories('hide_empty=0');

foreach ($of_categories_obj as $of_cat) {

    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}

$categories_tmp = array_unshift($of_categories, "Select a category:");    

       

//Access the WordPress Pages via an Array

$of_pages = array();

$of_pages_obj = get_pages('sort_column=post_parent,menu_order');    

foreach ($of_pages_obj as $of_page) {

    $of_pages[$of_page->ID] = $of_page->post_name; }

$of_pages_tmp = array_unshift($of_pages, "Select a page:");       



//Testing 

$of_options_select = array("one","two","three","four","five"); 

$of_options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");

$of_options_homepage_blocks = array( 

	"disabled" => array (

		"placebo" 		=> "placebo", //REQUIRED!

		"block_one"		=> "Block One",

		"block_two"		=> "Block Two",

		"block_three"	=> "Block Three",

	), 

	"enabled" => array (

		"placebo" => "placebo", //REQUIRED!

		"block_four"	=> "Block Four",

	),

);





//Stylesheets Reader

$alt_stylesheet_path = LAYOUT_PATH;

$alt_stylesheets = array();



if ( is_dir($alt_stylesheet_path) ) {

    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 

        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {

            if(stristr($alt_stylesheet_file, ".css") !== false) {

                $alt_stylesheets[] = $alt_stylesheet_file;

            }

        }    

    }

}



//Background Images Reader

$bg_images_path = STYLESHEETPATH. '/images/bg/'; // change this to where you store your bg images

$bg_images_url = get_bloginfo('template_url').'/images/bg/'; // change this to where you store your bg images

$bg_images = array();



if ( is_dir($bg_images_path) ) {

    if ($bg_images_dir = opendir($bg_images_path) ) { 

        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {

            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {

                $bg_images[] = $bg_images_url . $bg_images_file;

            }

        }    

    }

}



/*-----------------------------------------------------------------------------------*/

/* TO DO: Add options/functions that use these */

/*-----------------------------------------------------------------------------------*/



//More Options

$uploads_arr = wp_upload_dir();

$all_uploads_path = $uploads_arr['path'];

$all_uploads = get_option('of_uploads');

$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");

$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");

$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");



// Image Alignment radio box

$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 



// Image Links to Options

$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 





/*-----------------------------------------------------------------------------------*/

/* The Options Array */

/*-----------------------------------------------------------------------------------*/



// Set the Options Array

global $of_options;

$of_options = array();





$of_options[] = array( "name" => __("General Settings"),

                    "type" => "heading");

					



$of_options[] = array( "name" => __("Custom Favicon"),

					"desc" => __("Upload a 16px x 16px Png/Gif image that will represent your website's favicon."),

					"id" => "custom_favicon",

					"std" => "",

					"type" => "upload"); 



$of_options[] = array( "name" => __("Custom Logo"),

					"desc" => __("Upload a Png/Gif image that will represent your website's logo."),

					"id" => "custom_logo",

					"std" => "",

					"type" => "upload"); 

$of_options[] = array( "name" => __("Telephone &amp; Internet Left Content"),
					"desc" => __("Telephone &amp; Internet Left Content here"),
					"id" => "phonrinternet_left",
					"std" => "",
					"type" => "textarea");
					
$of_options[] = array( "name" => __("Telephone &amp; Internet Right Content"),
					"desc" => __("Telephone &amp; Internet Right Content here"),
					"id" => "phonrinternet_right",
					"std" => "",
					"type" => "textarea");
$of_options[] = array( "name" => __("Telephone &amp; Internet Insurance Content"),
					"desc" => __("Telephone &amp; Internet Insurance Content here"),
					"id" => "insurance_content",
					"std" => "",
					"type" => "textarea");



$of_options[] = array( "name" => __("Tracking Code"),

					"desc" => __("Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme."),

					"id" => "google_analytics",

					"std" => "",

					"type" => "textarea");     

					





$of_options[] = array( "name" => __("Home Content"),

					"type" => "heading");





$of_options[] = array( "name" => __("Header Right"),

					"desc" => __("Header right content here"),

					"id" => "header_right",

					"std" => 'The resource for everything a 457 visa applicants needs when working in Australia.Start by completing our 457 visa checklist',

					"type" => "textarea");  



					

$of_options[] = array( "name" => __("Banner Left"),

					"desc" => __("Banner left content here"),

					"id" => "banner_left",

					"std" => '<h2>457 Visa Checklist</h2>

<ul>

    <li><a href="#"><small>457 visa application</small> checklist</a></li>

    <li><a href="#"><small>Moving to Australia</small> checklist</a></li>

    <li><a href="#"><small>Updated for 2012</small> visa changes</a></li>

</ul>

<a href="#"><span><strong>View Checklist</strong></span></a>',

					"type" => "textarea");     





$of_options[] = array( "name" => __("Banner Right"),

					"desc" => __("Banner right content here"),

					"id" => "banner_right",

					"std" => '<h2>Compulsory 457 Health Insurance</h2>

<ul>

    <li><a href="#">Compulsory 457 Health Insurance</a></li>

    <li><a href="#">Moving to Australia checklist</a></li>

    <li><a href="#">Updated for 2012 visa changes</a></li>

</ul>

<a href="#"><span><strong>View Checklist</strong></span></a>',

					"type" => "textarea");    





$of_options[] = array( "name" => __("Home Box #1"),
					"desc" => __("Home 6 box content, content one here"),
					"id" => "box_one",
					"std" => '<h2>Bank Accounts</h2><p>View the most popular bank accounts for Australia\'s largest banks</p><a href="#">Compare Accounts</a>',
					"type" => "textarea");    


$of_options[] = array( "name" => __("Home Box #2"),
					"desc" => __("Home 6 box content, content two here"),
					"id" => "box_two",
					"std" => '<h2>Travel insurance</h2><p>View Australia\'s leading travel insurance policies for internations visitors.</p><a href="#">Get Quotes</a>',
					"type" => "textarea");    



$of_options[] = array( "name" => __("Home Box #3"),
					"desc" => __("Home 6 box content, content three here"),
					"id" => "box_three",
					"std" => '<h2>Telephone &#38; Internet</h2><p>Compare internet, mobile phone (sim) plans and more from major providers</p><a href="#">Get Quotes</a>',
					"type" => "textarea");    



$of_options[] = array( "name" => __("Home Box #4"),
					"desc" => __("Home 6 box content, content four here"),
					"id" => "box_four",
					"std" => '<h2>Migration Lawyers</h2><p>Compare Australia\'s best migration lawyers by services &#38; more!</p><a href="#">View Firms</a>',
					"type" => "textarea");    



$of_options[] = array( "name" => __("Home Box #5"),
					"desc" => __("Home 6 box content, content five here"),
					"id" => "box_five",
					"std" => '<h2>Currency Exchange</h2><p>View and compare live currency rates by forex exchange companies</p><a href="#">See Rates</a>',
					"type" => "textarea");    



$of_options[] = array( "name" => __("Home Box #6"),
					"desc" => __("Home 6 box content, content six here"),
					"id" => "box_six",
					"std" => '<h2>International Movers</h2><p>Compare international freight companies that deliver to Australia</p><a href="#">Get Started</a>',
					"type" => "textarea");    
	}
}

?>