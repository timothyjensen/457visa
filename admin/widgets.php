<?php

/*///////////////////////////////////////////////////////////////////// 
//  Twitter
/////////////////////////////////////////////////////////////////////*/


class NUE_Twitter extends WP_Widget {
 
	function NUE_Twitter() {
	
		global $ttrust_theme_name, $ttrust_version, $options;
		
        $widget_ops = array('classname' => 'widget_nue_Twitter', 'description' => 'Display latest tweets.');
		$this->WP_Widget('nue_Twitter', $theme_name.' '.__('Twitter'), $widget_ops);
    
    }
 
    function widget($args, $instance) {
    
    	global $theme_name, $ttrust_version, $options;
       
        extract( $args );
        
        $title	= empty($instance['title']) ? 'Latest Tweets' : $instance['title'];
        $user	= $instance['user'];        
        $label	= empty($instance['twitter_label']) ? 'Follow' : $instance['twitter_label'];
        if ( !$nr = (int) $instance['twitter_count'] )
			$nr = 5;
		else if ( $nr < 1 )
			$nr = 1;
		else if ( $nr > 15 )
			$nr = 15;
 
        ?>
			<?php echo $before_widget; ?>
				<?php echo $before_title . $title . $after_title; ?>
								
				<div id="twitterBox" class="clearfix"></div>

    			<script type="text/javascript">
 					//<![CDATA[
					jQuery(document).ready(function() {
						jQuery("#twitterBox").getTwitter({
							userName: '<?php echo $user; ?>',
							numTweets: '<?php echo $nr; ?>',
							loaderText: "Loading tweets...",
							slideIn: false,
							showHeading: false,
							headingText: "",
							showProfileLink: false
						});
					});
					//]]>    			
    			</script>				
				
				<?php if($label) : ?>
                <p class="twitterLink"><a class="action" href="http://twitter.com/<?php echo $user; ?>"><span><?php echo $label; ?></span></a></p>
                <?php endif; ?>
 
			<?php echo $after_widget; ?>
        <?php
    }

    function update($new_instance, $old_instance) {  
    
    	$instance['title'] = strip_tags($new_instance['title']);
    	$instance['user'] = strip_tags($new_instance['user']);    
    	$instance['twitter_label'] = strip_tags($new_instance['twitter_label']);
    	$instance['twitter_count'] = (int) $new_instance['twitter_count'];
                  
        return $new_instance;
    }
 
    function form($instance) {
    
    	global $theme_name, $theme_version, $options;
        
		$instance	= wp_parse_args( (array) $instance, array( 'title' => '', 'user' => '', 'twitter_link' => '', 'twitter_label' => '', 'twitter_count' => '') );
		$title		= empty($instance['title']) ? 'Latest Tweets' : $instance['title'];
		$user		= $instance['user'];		
		$label		= $instance['twitter_label'];
		if (!$count = (int) $instance['twitter_count']) $count = 5;
?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('user'); ?>"><?php _e('Username:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('user'); ?>" name="<?php echo $this->get_field_name('user'); ?>" type="text" value="<?php echo esc_attr($user); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('twitter_count'); ?>"><?php _e('Number of tweets:'); ?></label>
			<input id="<?php echo $this->get_field_id('twitter_count'); ?>" name="<?php echo $this->get_field_name('twitter_count'); ?>" type="text" value="<?php echo $count; ?>" size="3" /><br />
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('twitter_label'); ?>"><?php _e('Follow Link label:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('twitter_label'); ?>" name="<?php echo $this->get_field_name('twitter_label'); ?>" type="text" value="<?php echo esc_attr($label); ?>" />
			</label>
		</p>
		
<?php
	}

}
 
register_widget('NUE_Twitter');


/*///////////////////////////////////////////////////////////////////// 
//  Social
/////////////////////////////////////////////////////////////////////*/


class NUE_Social extends WP_Widget {
 
	function NUE_Social() {
	
		global $theme_name, $theme_version, $options;
		
        $widget_ops = array('classname' => 'widget_nue_Social', 'description' => 'Your Social Media.');
		$this->WP_Widget('NUE_Social', $ttrust_theme_name.' '.__('Social'), $widget_ops);
    
    }
 
    function widget($args, $instance) {
    
    	global $theme_name, $theme_version, $options;
       
        extract( $args );
        
        $title	= empty($instance['title']) ? 'Social' : $instance['title'];
		$facebook_url = $instance['facebook_url'] ;
		$twitter_url = $instance['twitter_url'];
		$youtube_url = $instance['youtube_url'];
		$linkedin_url = $instance['linkedin_url'];

        ?>
			<?php echo $before_widget; ?>
				<?php echo $before_title . $title . $after_title; ?>
								
				<div id="SocialBox" class="clearfix"></div>

    			<ul>
                	<?php if($facebook_url) : ?><li><a href="<?php echo $facebook_url ?>" target="_blank">Facebook</a></li><?php endif ?>
                    <?php if($twitter_url) : ?><li><a href="<?php echo $twitter_url ?>" target="_blank">Twitter</a></li><?php endif ?>
                    <?php if($youtube_url) : ?><li><a href="<?php echo $youtube_url ?>" target="_blank">Youtube</a></li><?php endif ?>
                    <?php if($linkedin_url) : ?><li><a href="<?php echo $linkedin_url ?>" target="_blank">Linkedin</a></li><?php endif ?>
                </ul>				
				
 
			<?php echo $after_widget; ?>
        <?php
    }

    function update($new_instance, $old_instance) {  
    
    	$instance['title'] = strip_tags($new_instance['title']);
		$instance['facebook_url'] = strip_tags($new_instance['facebook_url']);
		$instance['twitter_url'] = strip_tags($new_instance['twitter_url']);
		$instance['youtube_url'] = strip_tags($new_instance['youtube_url']);
		$instance['linkedin_url'] = strip_tags($new_instance['linkedin_url']);

        return $new_instance;
    }
 
    function form($instance) {
    
    	global $ttrust_theme_name, $ttrust_version, $options;
        
		$instance	= wp_parse_args( (array) $instance, array( 'title' => '', 'user' => '', 'Social_link' => '', 'Social_label' => '', 'Social_count' => '') );
		$title		= empty($instance['title']) ? 'Social' : $instance['title'];
		$facebook_url = $instance['facebook_url'] ;
		$twitter_url = $instance['twitter_url'];
		$youtube_url = $instance['youtube_url'];
		$linkedin_url = $instance['linkedin_url'];

?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('facebook_url'); ?>"><?php _e('Facebook URL:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('facebook_url'); ?>" name="<?php echo $this->get_field_name('facebook_url'); ?>" type="text" value="<?php echo esc_attr($facebook_url); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('twitter_url'); ?>"><?php _e('Twitter URL:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('twitter_url'); ?>" name="<?php echo $this->get_field_name('twitter_url'); ?>" type="text" value="<?php echo esc_attr($twitter_url); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('youtube_url'); ?>"><?php _e('Youtube URL:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('youtube_url'); ?>" name="<?php echo $this->get_field_name('youtube_url'); ?>" type="text" value="<?php echo esc_attr($youtube_url); ?>" />
			</label>
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id('linkedin_url'); ?>"><?php _e('Linkedin URL:'); ?>
			<input class="widefat" id="<?php echo $this->get_field_id('linkedin_url'); ?>" name="<?php echo $this->get_field_name('linkedin_url'); ?>" type="text" value="<?php echo esc_attr($linkedin_url); ?>" />
			</label>
		</p>		

		
<?php
	}

}
 
register_widget('NUE_Social');


?>