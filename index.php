<?php get_header(); ?>


        <div id="inner_content_area"><!--start content_area-->

            <div id="inner_content_wrapper">

            	<h1><?php if(is_category()) { single_cat_title(); } else { the_title(); }?></h1>
            	
                <div id="inner_content"><!--start content-->

                    <div class="inner_ltf_content">
						<?php if(is_category()) echo category_description(); ?>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>   
							<h2><?php the_title(); ?></h2>
                            <?php echo my_excerpts(); ?>

                        <?php endwhile; endif; ?>     

                    </div>

                    <div id="inner_widget_area">

                        <?php include(TEMPLATEPATH.'/sidebar-right.php') ?>

                    </div>

                </div><!--//end #content-->

            </div>    

        </div><!--//end #content_area-->

                  

                    

<?php get_footer(); ?>