<?php
/**
 * Template Name: 485 Visa template
 *
 */ 


if(isset($_POST['catID'])){
	switch($_POST['catID']){
		case 1:
			$page_red_id = 1719;
			break;
		case 2:
			$page_red_id = 2207;
			break;
		case 3:
			$page_red_id = 2209;
			break;
	}
	header("Location: " . get_permalink($page_red_id));
	die('');
}

$insurance_wrap = "";
$mypageID = $post->ID;
$_GET['catID'] = "1";

switch($post->ID){
	case 1719:
		$_GET['catID'] = 1; break;
	case 2207:
		$_GET['catID'] = 2; break;
	case 2209:
		$_GET['catID'] = 3; break;
}

get_header(); 
?>

        <div id="inner_content_area"><!--start content_area-->
        	<div id="inner-wrapper"><!--start inner-wrapper-->
                <div class="insurance-topcontent"><!--start insurance-topcontent-->
                    <div class="text-content">
                    
            	<?php 
            			$imgtitle = trim(get_post_meta($post->ID, 'top_img_text', true));
            			echo trim(get_post_meta($post->ID, 'custom_title', true));
            			
            			$bg_img_url = trim(get_post_meta($post->ID, 'bg_img_url', true)); 
            			if($bg_img_url != '') {
            				$bg_img_url = "background: url('" . $bg_img_url . "') no-repeat scroll 0% 0% transparent";
            				$bg_img_url = 'style="' . $bg_img_url . '"';
            			}
            			
            	?>
                    
               
                    <div class="selec-field">
                    	<form name="catSelect" action="" method="post" >
                    	<div class="rowElem rowElem1">
                            <label style="float:left; clear:both;">Cover for:</label><br />
                            <select name="catID" id="catID" onchange="catSelect.submit()">
                                <option value="1"<?php if($_GET['catID'] == '1') echo ' selected="selected"' ?>>Single</option>
                                <option value="2"<?php if($_GET['catID'] == '2') echo ' selected="selected"' ?>>Couple</option>
                                <option value="3"<?php if($_GET['catID'] == '3') echo ' selected="selected"' ?>>Family</option>
                            </select>
                        </div>
                        <div class="rowElem">
                         </div>
                       </form>
                    </div>
                </div>
                <img src="<?php echo trim(get_post_meta($post->ID, 'top_img_url', true)); ?>" width="300" height="230"  alt="<?php echo $imgtitle; ?>" title="<?php echo $imgtitle; ?>" />
                <div class="clear"></div>
            </div><!--//end #insurance-topcontent-->
        </div><!--//end #content_area-->
        <div class="clear"></div>
        
         <div id="insurance-outerwrap">
            <div id="insurance-wrap" class="<?php echo $insurance_wrap; ?>" <?php echo $bg_img_url; ?>>
                   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="insurancebox_content">
                   <?php the_content('More...'); ?>
                   </div>
                   <?php endwhile;endif;
                   wp_reset_query();
                   ?>
            </div>
            <div class="instructions">
            	<div class="instructions_inner">
            	<h4>This information is indicative and provided for general information only.<br />Please refer to the relevant policy brochure for full details including waiting
                 period.</h4>
                 <div class="instructions_one">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_tick.png" align="tick" /><span>This policy is a good match for this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_two">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_exclo.png" align="tick" /><span>This policy only covers some areas of this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_three">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_cross.png" align="tick" /><span>This policy does not offer comprehensive cover of this insurance item</span>
                 </div>
                 </div>
            </div>
            <div class="clear"></div>
                    
            <div class="insurance-btmcontent">
            		<div class="bankbox_bg"><div class="bankbox_topbg"><div class="bankbox_btmbg">
                        <div class="bank-entry">
                            <div class="block_ratingsbtmcontent">
                    			<?php the_block('ratingsbtmcontent'); ?>                     
                			</div>
							<?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
                        </div>
                    </div></div></div>
                </div>
       </div>
       </div>
       
<?php get_footer(); ?>