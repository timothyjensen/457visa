<?php global $data; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />	
	<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s' ), max( $paged, $page ) );
	?></title>
	<link href="<?php bloginfo('template_url') ?>/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen,projection" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link href="<?php bloginfo('template_url') ?>/css/css3.css" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_url') ?>/css/jqtransform.css" rel="stylesheet" type="text/css" />
    <?php /*?><link href="<?php bloginfo('template_url') ?>/css/ie-css.css" rel="stylesheet" type="text/css" /><?php */?>
	<link href="<?php bloginfo('template_url') ?>/responsive.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 10]>
   		<script src="<?php bloginfo('template_url') ?>/js/PIE.js" type="text/javascript"></script>
        <link href="<?php bloginfo('template_url') ?>/css/ie-css.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    
    <!-- custom favicon -->
	<?php $favicon_link = $data['custom_favicon']; if ( !$favicon_link ) $favicon_link = get_bloginfo('template_url') . '/favicon.ico'; ?>
	<link rel="shortcut icon" href="<?php echo $favicon_link; ?>" type="image/x-icon" />
    <?php wp_enqueue_script('jquery'); ?> 
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
	<script src="<?php bloginfo('template_url') ?>/js/highcharts.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jqplot/jquery.jqplot.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jqplot/plugins/jqplot.bubbleRenderer.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/js/jqplot/jquery.jqplot.css" />
	<?php wp_head(); ?>
    <script src="<?php bloginfo('template_url') ?>/js/init.js" type="text/javascript" async onload='myinit()'></script>
    <script src="<?php bloginfo('template_url') ?>/js/jquery.jqtransform.js" type="text/javascript" async onload='myinit()'></script>
    <script src="<?php bloginfo('template_url') ?>/js/jquery.qtip.js" type="text/javascript" async onload='myinit()'></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jcarousellite.js" async onload='myinit()'></script>
    
    <script src="<?php bloginfo('template_url') ?>/js/tinynav.min.js" type="text/javascript" async onload='myinit()'></script>
    <script type="text/javascript" async onload='myinit()'>
		jQuery(function () {
			jQuery('#menu-main-menu').tinyNav({
			header: 'Main Menu',
			active: 'selected',
			});
		});
    </script>

<!--[if lt IE 8]><style>
.second_column  span{
	display: inline-block!important;
	height: 100%;
	}
.first_column  span{
	display: inline-block!important;
	height: 100%;
	}
.sharethis_home iframe{
    margin-top:-40px!important;
}
</style><![endif]-->

<style>
.container{ width: 100%; }

.top_content_box_container{ padding-top: 10px; }

.center_content{ margin: 0 auto; }

.top_box_center{

	max-width: 980px;

	background: #fff;

	-webkit-border-radius: 6px;

	-moz-border-radius: 6px;

	-o-border-radius: 6px;

	-ms-border-radius: 6px;

	border-radius: 6px;

	border-left: 1px solid #d9d9d9;

	border-bottom: 2px solid #d9d9d9;
}

.top_content_box{

	margin: 0 18px;

	margin-bottom: 10px;

	padding-bottom: 20px;

	padding-top: 15px;

    max-width: 600px;

}

.top_content_box p{

	color: #313131;

    font-size: 14px;

    line-height: 18px;

    padding-bottom: 12px;

}

.bottom_content_box .about_feature_area {

	margin-top: 0;

}
.bottom_content_box.bank-entry{
	padding: 0;
	width: inherit !important;
}
.bottom_content_box.bank-entry h1{ height: inherit; }
.top_content_img{ float: right;}
.content_right_img {
	margin-right: 20px;
    position: relative;
    z-index: 100;
    margin-top: -88px;
}
.content_right_img_inn {
    background: #fff;
    border-radius: 6px;
    box-shadow: 0 0 5px 2px #dfdfdf;
    padding: 6px;
    margin-left: 15px;
}
.content_right_main_inn {
    box-shadow: 0 0 5px 2px #ededed inset;
    padding: 20px;
    max-width: 320px;
}
.bottom_content_box .about_feature_area{ overflow: visible;}
.clearfix{ clear: both;}


@media (max-width: 1023px) {

.top_box_center{ max-width: 660px; }

.top_content_box{ max-width: 100%; margin-right: 0; }

.bottom_content_box #about_content_wrapper,
#inner_content_area.bottom_content_box{ overflow: visible; }

.content_right_img { margin-top: -65px; }

.content_right_main_inn { padding: 20px 10px; }

.content_right_img { margin-right: 0; }
}

@media (max-width: 768px) {

	.top_box_center{ max-width: 440px; }

	/*.top_content_box{ max-width: 220px; }*/

	.bottom_content_box #about_content_wrapper,
	#inner_content_area.bottom_content_box{ overflow: hidden; }

	#inner_content_area.bottom_content_box{ padding-top: 25px !important; }

	.content_right_main_inn{ max-width: inherit !important; }

	.top_content_img{ float: none; margin-bottom: 20px; }

	.content_right_img_inn{ margin-left: 0; }

	.content_right_img { margin-top: 0; }

}

@media (max-width: 480px) {

	.top_box_center{ max-width: 300px; }

	/*.top_content_box{ max-width: 170px; }*/

	.top_content_img img{ max-width: 200px; height: auto;}

}
</style>   
	<?php include("config-tips.php"); ?>
    
    <script type="text/ecmascript">
		jQuery(function(){
			jQuery('.selec-field').jqTransform({imgPath:''});
			jQuery('.input_check').jqTransform({imgPath:''});
		});
		  jQuery.fn.qtip.styles.mystyle = { // Last part is the name of the style
		   width: 300,
		   padding: 10,
		   background: '#46B44A',
		   fontSize:12,
		   fontFamily:'Verdana',
		   color: '#fff',
		   textAlign: 'left',
		   
		  tip: { // Now an object instead of a string
			 corner: 'bottomMiddle', // We declare our corner within the object using the corner sub-option
			 color: '#46B44A',
			 size: {
				x: 20, // Be careful that the x and y values refer to coordinates on screen, not height or width.
				y : 8 // Depending on which corner your tooltip is at, x and y could mean either height or width!
			 }	 
		  },
		   border: {
			  width: 2,
			  radius: 1,
			  color: '#46B44A'
		   },
		   name: 'dark' // Inherit the rest of the attributes from the preset dark style
		}
		jQuery(document).ready(function() {
		<?php 
			global $tips;
			foreach($tips as $n => $v){
		?>
			jQuery('.icon<?php echo $n; ?>').qtip({
			   	content: "<?php echo $v; ?>",
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
		<?php } ?>
	
		});
	</script>
    
    
   <script type="text/javascript">
		jQuery(function() {
		jQuery("#testimonials").jCarouselLite({
			btnNext: ".next",
			btnPrev: ".prev",
			visible: 1,
			speed: 1600,
			auto: false
		});
		});
	</script>
<?php if(is_front_page()) { ?>
<meta property="og:title" content="457 Visa Compared - Australia's Leading 457 Visa Comparison Site!" />
<meta property="og:type" content="website" />
<meta property="og:description" content="Compare 457 visa services from Health Insurance through to Australian bank accounts and get free advice. Start comparing online today!" />
<meta property="og:url" content="http://
www.457visacompared.com.au/" />
<meta property="og:image" content="http://
www.457visacompared.com.au/wp-content/uploads/2012/10/logo.png" />
<meta property="og:site_name" content="457 Visa Compared" />
<meta property="og:locale" content="en_AU" />
<meta property="fb:admins" content="803902698" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@457visacom">
<meta name="twitter:creator" content="@457visacom">
<meta property="twitter:title" content="457 Visa Compared - Australia's Leading 457 Visa Comparison Site!" />
<meta property="twitter:description" content="Compare 457 visa services from Health Insurance through to Australian bank accounts and get free advice. Start comparing online today!" />
<meta property="twitter:image" content="http://
www.457visacompared.com.au/wp-content/uploads/2012/10/logo.png" />
<meta name="geo.region" content="AU-VIC" />
<meta name="geo.placename" content="Caulfield" />
<meta name="geo.position" content="-37.87248;145.023866" />
<meta name="ICBM" content="-37.87248, 145.023866" />
<meta name="news_keywords" content="457 visa health insurance news" />
<?php } ?>
</head>
<body <?php body_class(); ?>>

	<div id="wrapper"<?php if(is_page_template('template-internetphone.php') || is_home()) echo ''; else echo ' class="inner_wrapper"' ?>><!--start wrapper-->
    	<div id="header_area"><!--start header_area-->
        	<div id="header"><!--start header-->
            	<div id="top_header"><!--start top_header-->
					<?php $logo_link = $data['custom_logo']; if ( !$logo_link ) $logo_link = get_bloginfo('template_url') . '/images/logo.jpg'; ?>
                	<div class="logo">
                    	<a href="<?php bloginfo('url') ?>"><img src="<?php echo $logo_link; ?>" alt="457 Visa Compared" title="457 Visa Compared" width="492" height="105" /></a>
                    </div>
                    <div class="top_header_text">
                    	<p><?php echo do_shortcode(stripslashes($data['header_right'])); ?></p>
                    </div>
                </div><!--//end #top_header-->
                <div id="menu_bg"><!--start menu_bg-->
                <div id="menu_ltf"><!--start menu_ltf-->
                <div id="menu_rgt"><!--start menu_rgt-->
                	<div id="menu"><!--start menu-->
						<?php wp_nav_menu( array( 'container_id' =>  false, 'theme_location' => 'main_menu' , 'menu_class' => 'main_menu' , 'fallback_cb' => 'wp_page_menu' , 'depth' => 2 ) ); ?>
                    </div><!--//end #menu-->
                    <div id="search_field">
                    	<form action="<?php bloginfo('url') ?>" method="get">
                            <p class="search_input"><input type="text" value="Search our site" name="s" id="s" onfocus="if(this.value=='Search our site')(this.value='')" onblur="if(this.value=='')(this.value='Search our site')" /></p>
                            <p><input type="submit" value="" class="search_btn" /></p>
                        </form>
                    </div>
                </div><!--//end #menu_rgt-->
                </div><!--//end #menu_ltf-->
                </div><!--//end #menu_bg-->

                <div class="breadcrumb"><!--start breadcrumb-->
            		<?php if(function_exists(theme_breadcrumbs)) theme_breadcrumbs() ?>
            	</div><!--//end .breadcrumb-->            

        	</div><!--//end #header-->
            
            <?php if(is_front_page()) : ?>
            <div id="banner_area"><!--start banner_area-->
            	<div id="banner"><!--start banner-->
                	<div id="ltf_banner">
						<?php echo do_shortcode(stripslashes($data['banner_left'])); ?>
                    </div>
                    <div id="rgt_banner">
						<?php echo do_shortcode(stripslashes($data['banner_right'])); ?>
                    </div>
                </div><!--//end #banner-->
            </div><!--//end #banner_area-->
            <?php endif ?>
            
        </div><!--//end #header_area-->
        
        <?php if(is_front_page()) : ?>
        <div id="feature_area_wrapper"><!--start feature_area-->
        	<div  id="feature_area">
                <div id="feature_wrapper">
                    <div class="feature_post">
                    	<?php echo do_shortcode(stripslashes($data['box_one'])); ?>
                    </div>
                    <div class="feature_post">
                    	<?php echo do_shortcode(stripslashes($data['box_two'])); ?>
                    </div>
                    <div class="feature_post">
                    	<?php echo do_shortcode(stripslashes($data['box_three'])); ?>
                    </div>
                    <div class="feature_post">
						<?php echo do_shortcode(stripslashes($data['box_four'])); ?>
                    </div>
                    <div class="feature_post">
                    	<?php echo do_shortcode(stripslashes($data['box_five'])); ?>
                    </div>
                    <div class="feature_post">
                    	<?php echo do_shortcode(stripslashes($data['box_six'])); ?>
                    </div>
                </div>    
            </div>
        </div><!--//end #feature_area-->
		<?php endif ?>