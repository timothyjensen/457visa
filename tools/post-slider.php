<?php
/**
 * The slider/projects custom post type
 */

add_action('init', 'slider_register');
add_action('init', 'testimonial_register');


function slider_register() {	
	register_post_type( 'slider' , 
						array(
							'label' => 'Slider',
							'singular_label' => 'slider',
							'public' => true,
							'show_ui' => true,
							'capability_type' => 'post',
							'hierarchical' => false,
							'rewrite' => true,
							'menu_position' => 4,
							'menu_icon' => get_stylesheet_directory_uri() . '/img/generic.png',
							'supports' => array('title', 'revisions', 'thumbnail')
						)
					);
	
	register_taxonomy( 'slider_category', 
					   'slider', 
						array( 'hierarchical' => true, 
								'label' => __( 'Slider Category' ),
								'sort' => true,
								'args' => array( 'orderby' => 'term_order' ),
								'rewrite' => true
						)
					);  	
	
	
	add_filter('manage_edit-slider_columns', 'slider_edit_columns');
	add_action('manage_posts_custom_column',  'slider_custom_columns');
	function slider_edit_columns($columns){
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => 'Title',
			'slider_category' => 'Category',
			'slider_image' => 'Image',
		);
		return $columns;
	}
	function slider_custom_columns($column){
		switch ($column)
		{
			case "slider_category":  
				echo get_the_term_list($post->ID, 'slider_category', '', ', ','');  
				break;  			
			
			case 'slider_image':
				the_post_thumbnail( 'small_thumbnail' );
				break;
		}
	}
}



function testimonial_register() {	
	register_post_type( 'testimonials' , 
						array(
							'label' => 'Testimonials',
							'singular_label' => 'testimonials',
							'public' => true,
							'show_ui' => true,
							'capability_type' => 'post',
							'hierarchical' => false,
							'rewrite' => true,
							'menu_position' => 4,
							'menu_icon' => get_stylesheet_directory_uri() . '/img/generic.png',
							'supports' => array('title', 'editor')
						)
					);
	
	register_taxonomy( 'testimonials_category', 
					   'testimonials', 
						array( 'hierarchical' => true, 
								'label' => __( 'Testimonial Category' ),
								'sort' => true,
								'args' => array( 'orderby' => 'term_order' ),
								'rewrite' => true
						)
					);  	
	
	
	add_filter('manage_edit-testimonials_columns', 'testimonials_edit_columns');
	add_action('manage_posts_custom_column',  'testimonials_custom_columns');
	function testimonials_edit_columns($columns){
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => 'Title',
			'testimonials_category' => 'Category',
			'testimonials_description' => 'Description',
		);
		return $columns;
	}
	function testimonials_custom_columns($column){
		switch ($column)
		{
			case "testimonials_category":  
				echo get_the_term_list($post->ID, 'testimonials_category', '', ', ','');  
				break;  
				
			case 'testimonials_description':
				the_excerpt();  
				break;  
		}
	}
}



?>