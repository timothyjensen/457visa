<?php

// function: post_type BEGIN
function post_type()
{
	// Create The Labels (Output) For The Post Type
	$labels = 
	array(
		// The plural form of the name of your post type.
		'name' => __( 'Ratings'), 
		
		// The singular form of the name of your post type.
		'singular_name' => __('Ratings'),
		
		// The rewrite of the post type
		'rewrite' => 
			array(
				// prepends our post type with this slug
				'slug' => __( 'ratings' ) 
			),
			
		// The menu item for adding a new post.
		'add_new' => _x('Add New', 'ratings'), 
		
		// The header shown when editing a post.
		'edit_item' => __('Edit Ratings Item'),
		
		// Shown in the favourites menu in the admin header.
		'new_item' => __('New Ratings Item'), 
		
		// Shown alongside the permalink on the edit post screen.
		'view_item' => __('View Ratings'),
		
		// Button text for the search box on the edit posts screen.
		'search_items' => __('Search Ratings'), 
		
		// Text to display when no posts are found through search in the admin.
		'not_found' =>  __('No Ratings Items Found'),
		
		// Text to display when no posts are in the trash.
		'not_found_in_trash' => __('No Ratings Items Found In Trash'),
		 
		// Used as a label for a parent post on the edit posts screen. Only useful for hierarchical post types.
		'parent_item_colon' => '' 
	);
	
	// Set Up The Arguements
	$args = 
	array(
		// Pass The Array Of Labels
		'labels' => $labels, 
		
		// Display The Post Type To Admin
		'public' => true, 
		
		// Allow Post Type To Be Queried 
		'publicly_queryable' => true, 
		
		// Build a UI for the Post Type
		'show_ui' => true, 
		
		// Use String for Query Post Type
		'query_var' => true, 
		
		// Rewrite the slug
		'rewrite' => true, 
		
		// Set type to construct arguements
		'capability_type' => 'post', 
		
		// Disable Hierachical - No Parent
		'hierarchical' => false, 
		
		// Set Menu Position for where it displays in the navigation bar
		'menu_position' => null, 
		
		// Allow the ratings to support a Title, Editor, Thumbnail
		'supports' => 
			array(
				'title',
				'',
				'thumbnail'
			) 
	);
	
	// Register The Post Type
	register_post_type(__( 'ratings' ),$args);
	
	
} // function: post_type END


// function: ratings_messages BEGIN
function ratings_messages($messages)
{
	$messages[__( 'ratings' )] = 
		array(
			// Unused. Messages start at index 1
			0 => '',
			
			// Change the message once updated
			1 => sprintf(__('Ratings Updated. <a href="%s">View ratings</a>'), esc_url(get_permalink($post_ID))),
			
			// Change the message if custom field has been updated
			2 => __('Custom Field Updated.'),
			
			// Change the message if custom field has been deleted
			3 => __('Custom Field Deleted.'),
			
			// Change the message once ratings been updated
			4 => __('Ratings Updated.'),
			
			// Change the message during revisions
			5 => isset($_GET['revision']) ? sprintf( __('Ratings Restored To Revision From %s'), wp_post_revision_title((int)$_GET['revision'],false)) : false,
			
			// Change the message once published
			6 => sprintf(__('Ratings Published. <a href="%s">View Ratings</a>'), esc_url(get_permalink($post_ID))),
			
			// Change the message when saved
			7 => __('Ratings Saved.'),
			
			// Change the message when submitted item
			8 => sprintf(__('Ratings Submitted. <a target="_blank" href="%s">Preview Ratings</a>'), esc_url( add_query_arg('preview','true',get_permalink($post_ID)))),
			
			// Change the message when a scheduled preview has been made
			9 => sprintf(__('Ratings Scheduled For: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Ratings</a>'),date_i18n( __( 'M j, Y @ G:i' ),strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
			
			// Change the message when draft has been made
			10 => sprintf(__('Ratings Draft Updated. <a target="_blank" href="%s">Preview Ratings</a>'), esc_url( add_query_arg('preview','true',get_permalink($post_ID)))),
		);
	return $messages;	
	
} // function: ratings_messages END

// function: ratings_filter BEGIN
function ratings_filter()
{
	// Register the Taxonomy
	register_taxonomy(__( "filter" ), 
	
	
	// Assign the taxonomy to be part of the ratings post type
	array(__( "ratings" )), 
	
	// Apply the settings for the taxonomy
	array(
		"hierarchical" => true, 
		"label" => __( "Categories" ), 
		"singular_label" => __( "Categories" ), 
		"rewrite" => array(
				'slug' => 'filter', 
				'hierarchical' => true
				)
		)
	); 
} // function: ratings_filter END
register_taxonomy('ratings_tags', 'ratings', array( 'label' => 'Tags', 'hierarchical' => false));


add_action('init', 'post_type');
add_action( 'init', 'ratings_filter', 0 );
add_filter('post_updated_messages', 'ratings_messages');



?>