<?php
/**
 * The testimonials custom post type
 */
add_action('init', 'testimonials_register');

function testimonials_register() {	

	register_post_type( 'testimonials' , 
						array(
							'label' => 'Testimonials',
							'singular_label' => 'Testimonials',
							'public' => true,
							'show_ui' => true,
							'capability_type' => 'post',
							'hierarchical' => false,
							'rewrite' => true,
							'menu_position' => 4,
							'menu_icon' => get_stylesheet_directory_uri() . '/images/team.png',
							'supports' => array('title', 'editor', 'thumbnail')
						)
					);

	
	add_filter('manage_edit-testimonials_columns', 'testimonials_edit_columns');
	add_action('manage_posts_custom_column',  'testimonials_custom_columns');
	
	function testimonials_edit_columns($columns){
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => 'Title',
			'testimonials_description' => 'Description',
			'testimonials_image' => 'Image',
		);
	
		return $columns;
	}
	
	function testimonials_custom_columns($column){
		switch ($column)
		{

			case 'testimonials_description':
				the_excerpt();  
				break;  

			case 'testimonials_image':
				the_post_thumbnail( 'small-admin-post-thumbnail' );
				break;
		}
	}
}

?>