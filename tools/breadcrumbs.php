<?php



function theme_breadcrumbs() {

 

  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show

  $delimiter = '&nbsp;<span>&raquo;</span>&nbsp; '; // delimiter between crumbs

  $home = 'Home'; // text for the 'Home' link

  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show

  $before = ''; // tag before the current crumb

  $after = ''; // tag after the current crumb

 

  global $post;

  $homeLink = get_bloginfo('url');

 

  if (is_home() || is_front_page()) {

 

    if ($showOnHome == 0) echo 'You are here'. $delimiter .'<a href="' . $homeLink . '">' . $home . '</a>';

 

  } else {

 
	if ( is_page() && $post->post_parent) echo '<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . $homeLink . '"><span itemprop="title">' . $home . '</span></a> > &nbsp;</div>';
	//else echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '';
	else echo '<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . $homeLink . '"><span itemprop="title">' . $home . '</span></a> > &nbsp;</div>';

 

    if ( is_category() ) {

      global $wp_query;

      $cat_obj = $wp_query->get_queried_object();

      $thisCat = $cat_obj->term_id;

      $thisCat = get_category($thisCat);

      $parentCat = get_category($thisCat->parent);

      if ($thisCat->parent != 0) echo ''.(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ')).'';

      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

 

    } elseif ( is_day() ) {

      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';

      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';

      echo $before . get_the_time('d') . $after;

 

    } elseif ( is_month() ) {

      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';

      echo $before . get_the_time('F') . $after;

 

    } elseif ( is_year() ) {

      echo $before . get_the_time('Y') . $after;

 

    } elseif ( is_single() && !is_attachment() ) {

      if ( get_post_type() != 'post' ) {

        $post_type = get_post_type_object(get_post_type());

        $slug = $post_type->rewrite;

        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';

        if ($showCurrent == 1) echo $before . get_the_title() . $after;

      } else {

        $cat = get_the_category(); $cat = $cat[0];

        echo ''.str_replace( "Featured", "Surf", get_category_parents( $cat, TRUE, ' ' . $delimiter . ' ' ).'' );

        if ( $showCurrent == 1 ) echo $before . get_the_title() . $after;

      }

 

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

      $post_type = get_post_type_object(get_post_type());

      echo $before . $post_type->labels->singular_name . $after;

 

    } elseif ( is_attachment() ) {

/*
      $parent = get_post($post->post_parent);

      $cat = get_the_category($parent->ID); $cat = $cat[0];

      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');

      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';

      if ($showCurrent == 1) echo $before . get_the_title() . $after;
*/
 

    } elseif ( is_page() && !$post->post_parent ) {

      if ($showCurrent == 1) echo $before . get_the_title($page->ID) . $after;

 

    } elseif ( is_page() && $post->post_parent ) {

      $parent_id  = $post->post_parent;

      $breadcrumbs = array();

      while ($parent_id) {

        $page = get_page($parent_id);

        $breadcrumbs[] = '<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . get_permalink($page->ID) . '"><span itemprop="title">' . get_the_title($page->ID) . '</span></a> > &nbsp;</div>';

        $parent_id  = $page->post_parent;

      }

      $breadcrumbs = array_reverse($breadcrumbs);

      //foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ';

      if ($showCurrent == 1) {
      	//echo $before . get_the_title() . $after;
      	//echo '<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . get_permalink() . '"><span itemprop="title">' . get_the_title() . '</span></a></div>';
      	echo '<div><a href="' . get_permalink() . '"><span>' . get_the_title() . '</span></a></div>';
      }

 

    } elseif ( is_search() ) {

      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

 

    } elseif ( is_tag() ) {

      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

 

    } elseif ( is_author() ) {

       global $author;

      $userdata = get_userdata($author);

      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

 

    } elseif ( is_404() ) {

      echo $before . 'Error 404' . $after;

    }

 

    if ( get_query_var('paged') ) {

/*      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';

      echo __('Page') . ' ' . get_query_var('paged');

      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';

*/    }

 

    echo '';

 

  }

} // end breadcrumbs()





?>