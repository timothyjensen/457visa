<?php
//A simple function to get data stored in a custom field

if(!function_exists('get_custom_field')) {
function get_custom_field($field) {
	global $post;
	$custom_field = get_post_meta($post->ID, $field, true);
	echo $custom_field;
}
}

// Adds a custom section to the "advanced" Post and Page edit screens
function sp_add_custom_box() {
	if( function_exists( 'add_meta_box' )) {
		add_meta_box( 'sp_custom_box_1', __( 'Customize Health Insurance Page', 'sp'), 'sp_inner_custom_box_1', 'page','normal', 'high' );
		add_meta_box( 'sp_custom_box_2', __( 'Custom Field', 'sp'), 'sp_inner_custom_box_2', 'ratings','normal', 'high' );
	}
}

/* Prints the inner fields for the custom post/page section */
function sp_inner_custom_box_1() {
	global $post;
	
	// Use nonce for verification ... ONLY USE ONCE!
	echo '<input type="hidden" name="sp_noncename" id="sp_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// The actual fields for data entry
	if(get_post_meta($post->ID, '_people_position', true))
		$people_position = get_post_meta($post->ID, '_people_position', true);
	else
		$people_position = ' ';	
	
	if(get_post_meta($post->ID, '_twitter_url', true))
		$twitter_url = get_post_meta($post->ID, '_twitter_url', true);
	else
		$twitter_url = ' ';

	if(get_post_meta($post->ID, '_insurance_text', true))
		$insurance_text = get_post_meta($post->ID, '_insurance_text', true);
	else
		$insurance_text = ' ';


	echo "<div style='overflow:hidden;'>";
	echo '<p style="float:left;width:170px"><label for="_insurance_text">' . __("Enter Health Insurance Table Bottom Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><textarea id= "_insurance_text" name="_insurance_text" rows="5" cols="77">'.$insurance_text.'</textarea><br /></p>';
	echo '<br style="clear:both;" />';
	echo '</div>';

}


function sp_inner_custom_box_2() {
	global $post;
	
	// Use nonce for verification ... ONLY USE ONCE!
	echo '<input type="hidden" name="sp_noncename" id="sp_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	// The actual fields for data entry
	if(get_post_meta($post->ID, '_header_logo', true))
		$header_logo = get_post_meta($post->ID, '_header_logo', true);
	else
		$header_logo = ' ';	
	
	
	if(get_post_meta($post->ID, '_monthly_price', true))
		$monthly_price = get_post_meta($post->ID, '_monthly_price', true);
	else
		$monthly_price = ' ';	
		
	if(get_post_meta($post->ID, '_monthly_price', true))
		$monthly_price = get_post_meta($post->ID, '_monthly_price', true);
	else
		$monthly_price = ' ';	
		
	if(get_post_meta($post->ID, '_icon_field01', true))
		$icon_field01 = get_post_meta($post->ID, '_icon_field01', true);
	else
		$icon_field01 = ' ';	
		
	if(get_post_meta($post->ID, '_icon_field02', true))
		$icon_field02 = get_post_meta($post->ID, '_icon_field02', true);
	else
		$icon_field02 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field03', true))
		$icon_field03 = get_post_meta($post->ID, '_icon_field03', true);
	else
		$icon_field03 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field04', true))
		$icon_field04 = get_post_meta($post->ID, '_icon_field04', true);
	else
		$icon_field04 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field05', true))
		$icon_field05 = get_post_meta($post->ID, '_icon_field05', true);
	else
		$icon_field05 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field06', true))
		$icon_field06 = get_post_meta($post->ID, '_icon_field06', true);
	else
		$icon_field06 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field07', true))
		$icon_field07 = get_post_meta($post->ID, '_icon_field07', true);
	else
		$icon_field07 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field08', true))
		$icon_field08 = get_post_meta($post->ID, '_icon_field08', true);
	else
		$icon_field08 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field09', true))
		$icon_field09 = get_post_meta($post->ID, '_icon_field09', true);
	else
		$icon_field09 = ' ';
		
	if(get_post_meta($post->ID, '_icon_field10', true))
		$icon_field10 = get_post_meta($post->ID, '_icon_field10', true);
	else
		$icon_field10 = ' ';
		
	if(get_post_meta($post->ID, '_visitsite_link', true))
		$visitsite_link = get_post_meta($post->ID, '_visitsite_link', true);
	else
		$visitsite_link = ' ';
		
	if(get_post_meta($post->ID, 'best_value', true))
		$checked = 'checked="checked"';
	else
		$checked = ' ';

	

	echo "<div style='overflow:hidden;'>";
	echo '<p style="float:left;width:170px"><label for="_header_logo">' . __("Enter Header Logo URL :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_header_logo" name="_header_logo" value="'.$header_logo.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
		
	echo '<p style="float:left;width:170px"><label for="_monthly_price">' . __("Enter Monthly Price :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_monthly_price" name="_monthly_price" value="'.$monthly_price.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';

	echo '<p style="float:left;width:170px"><label for="_icon_field01">' . __("Enter Meets 457 Visa Requirements Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field01" name="_icon_field01" value="'.$icon_field01.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field02">' . __("Enter Public Hospital Accommodation Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field02" name="_icon_field02" value="'.$icon_field02.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field03">' . __("Enter Private Hospital Accommodation Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field03" name="_icon_field03" value="'.$icon_field03.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field04">' . __("Enter Out Patient Following Hospitalisation Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field04" name="_icon_field04" value="'.$icon_field04.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field05">' . __("Enter Visits To A GP Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field05" name="_icon_field05" value="'.$icon_field05.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field06">' . __("Enter Medical Repatriation Cover Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field06" name="_icon_field06" value="'.$icon_field06.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field07">' . __("Enter Maternity Services  Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field07" name="_icon_field07" value="'.$icon_field07.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field08">' . __("Enter Emergency Ambulance Cover Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field08" name="_icon_field08" value="'.$icon_field08.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field09">' . __("Enter Extra's Cover (eg Dentist) Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field09" name="_icon_field09" value="'.$icon_field09.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_icon_field10">' . __("Enter Other important elements Content :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_icon_field10" name="_icon_field10" value="'.$icon_field10.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo '<p style="float:left;width:170px"><label for="_visitsite_link">' . __("Enter Visit Website Link :", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="text" id= "_visitsite_link" name="_visitsite_link" value="'.$visitsite_link.'" size="80" /><br /></p>';
	echo '<br style="clear:both;" />';
	
	echo "<div style='overflow:hidden;'>";
	echo '<p style="float:left"><label for="best_value">' . __("<strong>Show Best Value</strong>  : &nbsp;", 'sp' ) . '</label></p>';
	echo '<p style="float:left"><input type="checkbox" id= "best_value" name="best_value"'. $checked .'size="15" /><br /></p></div>';

	echo '</div>';

}


/* When the post is saved, saves our custom data */
function sp_save_postdata($post_id, $post) {
	
	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['sp_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post->ID ))
		return $post->ID;
	} else {
		if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;
	}

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.
	
	$mydata['_header_logo'] = $_POST['_header_logo'];
	$mydata['_monthly_price'] = $_POST['_monthly_price'];
	$mydata['_insurance_text'] = $_POST['_insurance_text'];
	
	$mydata['_icon_field01'] = $_POST['_icon_field01'];
	$mydata['_icon_field02'] = $_POST['_icon_field02'];
	$mydata['_icon_field03'] = $_POST['_icon_field03'];
	$mydata['_icon_field04'] = $_POST['_icon_field04'];
	$mydata['_icon_field05'] = $_POST['_icon_field05'];
	$mydata['_icon_field06'] = $_POST['_icon_field06'];
	$mydata['_icon_field07'] = $_POST['_icon_field07'];
	$mydata['_icon_field08'] = $_POST['_icon_field08'];
	$mydata['_icon_field09'] = $_POST['_icon_field09'];
	$mydata['_icon_field10'] = $_POST['_icon_field10'];
	
	$mydata['_visitsite_link'] = $_POST['_visitsite_link'];
	$mydata['best_value'] = $_POST['best_value'];
	
	
	// Add values of $mydata as custom fields
	
	foreach ($mydata as $key => $value) { //Let's cycle through the $mydata array!
		if( $post->post_type == 'revision' ) return; //don't store custom data twice
		$value = implode(',', (array)$value); //if $value is an array, make it a CSV (unlikely)
		if(get_post_meta($post->ID, $key, FALSE)) { //if the custom field already has a value
			update_post_meta($post->ID, $key, $value);
		} else { //if the custom field doesn't have a value
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key); //delete if blank
	}

}

/* Use the admin_menu action to define the custom boxes */
add_action('admin_menu', 'sp_add_custom_box');

/* Use the save_post action to do something with the data entered */
add_action('save_post', 'sp_save_postdata', 1, 2); // save the custom fields

/**
* Add our thick box hijacking script to the header
*/
function cd_people_enqueue()
{
	wp_enqueue_script( 'cdog-thickbox', get_bloginfo('template_url') . '/js/thickbox-hijack.js', array(), NULL );
}

add_action('admin_print_scripts', 'cd_people_enqueue');




?>