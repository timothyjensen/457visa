<?php 
get_header(); ?>



        <div id="inner_content_area"><!--start content_area-->

            <div id="inner_content_wrapper">

            	<h1><?php _e( 'Error - 404 : Not Found'); ?></h1>

                <div id="inner_content"><!--start content-->

                    <div class="inner_ltf_content">

                        <p><?php _e( 'Apologies, but the page you requested could not be found. '); ?></p>

                    </div>

                    <div id="inner_widget_area">

                        <?php include(TEMPLATEPATH.'/sidebar-right.php') ?>

                    </div>

                </div><!--//end #content-->

            </div>    

        </div><!--//end #content_area-->

                  

                    

<?php get_footer(); ?>