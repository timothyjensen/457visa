<?php get_header(); ?>

		<div id="content_area"><!--start content_area-->
        	<div id="content"><!--start content-->
            	<div class="ltf_content">
					<?php
                        $args=array(
                        'pagename'=>'home',
                        );	
                        query_posts($args);
                    ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); $custom_title = trim(get_post_meta($post->ID, 'custom_title', true)) ?>
                    	<h2><?php if($custom_title) echo $custom_title; else echo get_the_title() ?></h2>
                        <?php the_content('More...'); ?>
                    <?php endwhile;endif; wp_reset_query(); ?>
                    
                    
					<?php show_social_media(get_bloginfo('url'), get_the_title(), "sharethis_home"); ?>
                    

                </div>
                
                <div class="widget_area">
                	<h1>Latest 457 Visa Information</h1>
                    <div class="widget">
                    
					<?php
                        $limit = 4;
                        $args=array(
                        'posts_per_page'=> $limit,
                        );	
                        query_posts($args);
                        global $more;
                        $more = 0; 				
                    ?>

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="widget_post">
                        	<a href="<?php the_permalink() ?>"><?php if(has_post_thumbnail()) the_post_thumbnail('small_post_thumb', 'title='.trim(strip_tags( $post->post_title ))); ?></a>
                            <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        </div>
					<?php endwhile; endif; wp_reset_query(); ?>
                    
                    </div>
                    
                </div>
            </div><!--//end #content-->
        </div><!--//end #content_area-->
                  
                    
<?php get_footer(); ?>