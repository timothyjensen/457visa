<?php get_header(); ?>

        <div id="inner_content_area"><!--start content_area-->
            <div id="inner_content_wrapper">
            	<h1>Search Results</h1>
                <div id="inner_content"><!--start content-->
                    <div class="inner_ltf_content">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>  
                        
                        	<div class="post">
                                <h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2> 
                                <?php echo my_excerpts() ?>
                            </div>
						<?php endwhile; ?>
							<?php if ( $wp_query->max_num_pages > 1 ) : ?>
                            <hr />
                            <div id="pagination">
                                <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>  
                            </div>
                            <?php endif ?>
                        <?php else : ?>
                        <h2 class="center"><?php _e( 'No Post Found'); ?></h2>
                    	<?php endif; ?>  

                    </div>
                    <div id="inner_widget_area">
                        <?php include(TEMPLATEPATH.'/sidebar-right.php') ?>
                    </div>
                </div><!--//end #content-->
            </div>    
        </div><!--//end #content_area-->
                  
                    
<?php get_footer(); ?>