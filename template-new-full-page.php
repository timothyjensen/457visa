<?php
/**
 * Template Name: New Full Page template
 *
 */ 
get_header(); ?>
        
        <div class="container top_content_box_container">
            <div class="center_content top_box_center">
                <div class="top_content_box">
                    <?php the_block('aboutbtmcontent'); ?>
                </div>
            </div>
        </div>

        <div id="inner_content_area" class="bottom_content_box bank-entry"><!--start content_area-->
            <div id="about_content_wrapper">
                <div class="top_about_box"><div class="btm_about_box"><div class="about_box_bg">
                    
                    <div class="about_feature_area">
                    <div class="block_aboutbtmcontent">
                        <div class="top_content_img">
                            <div class="content_right_img">
                                <div class="content_right_img_inn">
                                    <div class="content_right_main_inn">
                                        <?php if(has_post_thumbnail()){ the_post_thumbnail( 'medium' ); } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (have_posts()){ the_post(); the_content(); } ?>
                        <div class="clearfix" ></div>
                    </div>
                    <?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
                    </div>
                </div></div></div>
            </div>    
        </div><!--//end #content_area-->
                  
                    
<?php get_footer(); ?>