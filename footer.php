<?php global $data; ?>





        <div id="footer_area"><!--start footer_area-->

        	<div id="footer"><!--start footer-->

                <div id="nav">

					<?php wp_nav_menu( array( 'container_id' =>  false, 'theme_location' => 'footer_menu' , 'menu_class' => 'footer_menu' , 'fallback_cb' => 'wp_page_menu' , 'depth' => 1 ) ); ?>

                </div>
                <p>&nbsp;</p>
				<p>&nbsp;&nbsp;&nbsp;Australia's Leading 457 visa comparison website, 457visacompared. Copyright 2018, Innovate Online. All Rights Reserved.</p>
            </div><!--//end #footer-->
			
        </div><!--//end #footer_area-->

    </div><!--//end #wrapper-->

    

<?php wp_footer(); ?>
<?php /*

*/ ?>
<script src="<?php bloginfo('template_url') ?>/js/fixed.js" type="text/javascript"></script>
 <script type="text/javascript">
	jQuery(function() {
		jQuery("#table_direction .lft_direction").hide();
		jQuery("#table_direction .rgt_direction").show();	
		
		jQuery("#table_direction .lft_direction").click(function(){
			jQuery(".table_part2").fadeOut('slow').hide();
			jQuery(".table_part1").fadeIn('slow').show();
			jQuery("#table_direction .lft_direction").hide();
			jQuery("#table_direction .rgt_direction").show();
			jQuery(".head_table1").css('display','block');
			jQuery(".head_table2").css('display','none');
			
		});
		
		jQuery("#table_direction .rgt_direction").click(function(){
			jQuery(".table_part1").fadeOut('slow').hide();
			jQuery(".table_part2").fadeIn('slow').show().css("display:block");
			jQuery("#table_direction .rgt_direction").hide();      
			jQuery("#table_direction .lft_direction").show();
			
			jQuery(".head_table1").css('display','none');
			jQuery(".head_table2").css('display','block');
		});
		document.addEventListener("scroll", windowScroll, false);
	});
	jQuery(window).resize(function() {
		var win_width = jQuery(window).width();
		if (win_width > 480) {
			jQuery(".table_part1").show();
			jQuery(".table_part2").show();
		}
		if (win_width <= 480) {
			jQuery(".table_part2").hide();
		}
	});
	jQuery(function() { jQuery("#table_direction").stick_in_parent();});
		
	jQuery(window).scroll(function() { windowScroll() });	
	
	
	
	function windowScroll() {
		fixed_top = jQuery('#removed_fixed_arrow').offset().top;
		total_top = jQuery("body").scrollTop() + 200;
		var win_width = jQuery(window).width();

		
		if (win_width <= 480) { 
			if(total_top >= fixed_top) jQuery("#table_direction").hide();
			else jQuery("#table_direction").show()
		}
	}

  </script>
</body>

</html>                                                                                         