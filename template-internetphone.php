<?php
/**
 * Template Name: Internet Phone template
 *
 */
get_header(); ?>

            <div id="banner_area"><!--start banner_area-->
            	<div id="banner-ip"><!--start banner-->
                	<h1>Telephone &amp; Internet</h1>
                	<div id="ltf_banner-ip">
                    	<?php echo do_shortcode(stripslashes($data['phonrinternet_left'])); ?>
                    </div>
                    <div id="rgt_banner-ip">
                    <?php echo do_shortcode(stripslashes($data['phonrinternet_right'])); ?>
                    </div>
                </div><!--//end #banner-->
            </div><!--//end #banner_area-->
        </div><!--//end #header_area-->
        
        <?php /*
        <div id="feature_area_wrapper"><!--start feature_area-->
        	<div  id="feature_area">
                <div id="feature_wrapper">
                    <div class="feature_bg"><div class="feature_top"><div class="feature_bottom">
                        <div class="feature travel_insurance">
                            <?php echo do_shortcode(stripslashes($data['insurance_content'])); ?>
                        </div>
                    </div></div></div>
                    
                     <div class="feature_bg ipfeature_bg"><div class="feature_top ipfeature_top"><div class="feature_bottom ipfeature_bottom">
                        <div class="feature ipfeature">
                        
                        
                            
                            <div id="testimonials" class="jCarouselLite">
                			<h2>Why should I consider switching my energy provider?</h2>
                        	<div class="testtimonial_arrowholder">
                                <div class="next"><a href="#"></a></div>
                                <div class="prev"><a href="#"></a></div>
                            </div>
                            <div class="main_testimonial">
                            <ul>
                            	<?php 
									$args=array(
									'post_type'=>'testimonials',
									'posts_per_page' => -1,
								);
								query_posts($args);
								if (have_posts()) : while (have_posts()) : the_post(); ?>
                            	<li>
                                	<blockquote>
                                    	<?php the_content('more...') ; ?> 
                                    </blockquote>
                                </li>
                                <?php endwhile; endif; wp_reset_query();?>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div></div></div>
                     
                    
                    
                    
                </div>    
            </div>
        </div><!--//end #feature_area-->

        <div id="content_area"><!--start content_area-->
        	<div id="content"><!--start content-->
            	<div class="ltf_content">
                	<?php if (have_posts()) : while (have_posts()) : the_post(); $custom_title = trim(get_post_meta($post->ID, 'custom_title', true)) ?>
                	<h2><?php if($custom_title) echo $custom_title; else echo get_the_title() ?></h2>
                    <?php the_content('More...'); ?>
                    <?php endwhile; endif; ?>
                </div>
                <div class="widget_area">
                	<h2>Latest 457 Visa Information</h2>
                    <div class="widget">
                    
					<?php
                        $limit = 4;
                        $args=array(
                        'posts_per_page'=> $limit,
                        );	
                        query_posts($args);
                        global $more;
                        $more = 0; 				
                    ?>

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="widget_post">
                        	<a href="<?php the_permalink() ?>"><?php if(has_post_thumbnail()) the_post_thumbnail('small_post_thumb', 'title='.trim(strip_tags( $post->post_title ))); ?></a>
                            <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        </div>
					<?php endwhile; endif; wp_reset_query(); ?>
                    
                    </div>
                    
                </div>
            </div><!--//end #content-->
        </div><!--//end #content_area-->
        */ ?>         
        
        
        <div id="content_area"><!--start content_area-->
        	<div id="content"><!--start content-->
            	<div class="ltf_content">
                	<?php if (have_posts()) : while (have_posts()) : the_post(); $custom_title = trim(get_post_meta($post->ID, 'custom_title', true)) ?>
                	<h2><?php if($custom_title) echo $custom_title; else echo get_the_title() ?></h2>
                    <?php the_content('More...'); ?>
                    <?php endwhile; endif; ?>
                </div>
                
                <div class="widget_area">
                	<h2>Latest 457 Visa Information</h2>
                    <div class="widget">
                    
					<?php
                        $limit = 4;
                        $args=array(
                        'posts_per_page'=> $limit,
                        );	
                        query_posts($args);
                        global $more;
                        $more = 0; 				
                    ?>

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="widget_post">
                        	<a href="<?php the_permalink() ?>"><?php if(has_post_thumbnail()) the_post_thumbnail('small_post_thumb', 'title='.trim(strip_tags( $post->post_title ))); ?></a>
                            <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                        </div>
					<?php endwhile; endif; wp_reset_query(); ?>
                    
                    </div>
                
            </div><!--//end #content-->
        </div><!--//end #content_area-->
        
        
                    
<?php get_footer(); ?>