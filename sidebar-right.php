					<div class="inner_widget">
                    <?php
					
						$useful_url = get_post_meta($post->ID, 'useful_url', false); 

						if($useful_url) :
							echo '<h2>Useful Links</h2>';
							echo '<ul>';
							foreach($useful_url as $url) {
										echo '<li>'.$url.'</li>';
							} 
							echo '</ul>';
						else :
					
					?>
                    
                    	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar') ) : ?><?php endif ?>  
                        
                        
                    <?php endif ?>
                        
                    </div>
