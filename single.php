<?php get_header(); ?>



        <div id="inner_content_area"><!--start content_area-->

            <div id="inner_content_wrapper">

            	<h1><?php the_title() ?></h1>

                <div id="inner_content"><!--start content-->

                    <div class="inner_ltf_content">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>   

                            <?php the_content() ?>

                        <?php endwhile; endif; ?>     
						
						<?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
						
                    </div>

                    <div id="inner_widget_area">

                        <?php include(TEMPLATEPATH.'/sidebar-right.php') ?>

                    </div>

                </div><!--//end #content-->

            </div>    

        </div><!--//end #content_area-->

                  

                    

<?php get_footer(); ?>