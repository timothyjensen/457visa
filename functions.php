<?php
// This was not meant not to replace your functions.php file. Just copy and paste the codes below into your own functions.php file.
/*-----------------------------------------------------------------------------------*/
// Options Framework
/*-----------------------------------------------------------------------------------*/
// Paths to admin functions
define('ADMIN_PATH', STYLESHEETPATH . '/admin/');
define('ADMIN_DIR', get_template_directory_uri() . '/admin/');
define('LAYOUT_PATH', ADMIN_PATH . '/layouts/');
// You can mess with these 2 if you wish.
$themedata = get_theme_data(STYLESHEETPATH . '/style.css');
define('THEMENAME', $themedata['Name']);
define('OPTIONS', 'of_options'); // Name of the database row where your options are stored
// Build Options
require_once (ADMIN_PATH . 'admin-interface.php');		// Admin Interfaces
require_once (ADMIN_PATH . 'theme-options.php'); 		// Options panel settings and custom settings
require_once (ADMIN_PATH . 'admin-functions.php'); 	// Theme actions based on options settings
require_once (ADMIN_PATH . 'medialibrary-uploader.php'); // Media Library Uploader
// Add RSS Feed Links
//add_theme_support( 'automatic-feed-links' );
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 200, 150, true );
add_image_size('small_thumbnail', 150, 112, true);
add_image_size('small_post_thumb', 80, 60, true);
add_image_size('ratings_thumbnail', 300, 230, true);
add_image_size('phone_thumbnail', 160, 230, true);
// Custom Menus
add_theme_support('nav_menus');
register_nav_menu('main_menu', __('Main Menu'));
register_nav_menu('footer_menu', __('Footer Menu'));
// Include Custom Theme Widgets
// unregister all default WP Widgets
function unregister_default_wp_widgets() {
	unregister_widget('WP_Widget_Calendar');
	unregister_widget('WP_Widget_Search');
	unregister_widget('WP_Widget_RSS');
	unregister_widget('WP_Widget_Akismet');
}

add_action('widgets_init', 'unregister_default_wp_widgets', 1);
// Feed Refresh Rate
add_filter( 'wp_feed_cache_transient_lifetime', function() {
    return 1800;
} );

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Sidebar',
		'description' => __('Sidebar'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<span class="widget_title">',
		'after_title' => '</span>',
	));
	register_sidebar(array(
		'name' => 'Health Insurance Table Widget',
		'description' => __('Health Insurance Table Content Here'),
		'before_widget' => '<div id="%1$s" class="%2$s insurance-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name' => 'Health Insurance German Table Widget',
		'description' => __('Health Insurance German Table Content Here'),
		'before_widget' => '<div id="%1$s" class="%2$s insurance-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
	register_sidebar(array(
		'name' => '485 Health Insurance Table Widget',
		'description' => __('485 Health Insurance Table Content Here'),
		'before_widget' => '<div id="%1$s" class="%2$s insurance-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));

	register_sidebar(array(
		'name' => 'Life Insurance Table Widget',
		'description' => __('Life Insurance Table Widget Content Here'),
		'before_widget' => '<div id="%1$s" class="%2$s insurance-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));

	register_sidebar(array(
		'name' => 'Overseas Insurance Table Widget',
		'description' => __('Overseas Insurance Table Widget Content Here'),
		'before_widget' => '<div id="%1$s" class="%2$s insurance-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}
function get_page_id($page_name){
	global $wpdb;
	$page_name = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."' AND post_status = 'publish' AND post_type = 'page'");
	return $page_name;
}
// Custom Excerpt Length
function new_excerpt_length($length) {
	return 55;
}
add_filter('excerpt_length', 'new_excerpt_length');
function new_excerpt_more($more) {
	return ' <a href="'. esc_url( get_permalink() ) . '">' . __( '' ) . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
function my_excerpts($content = false) {
	global $post;
	$mycontent = $post->post_excerpt;
	$mycontent = $post->post_content;
	$mycontent = strip_shortcodes($mycontent);
	$mycontent = str_replace(']]>', ']]&gt;', $mycontent);
	$mycontent = strip_tags($mycontent);
	$excerpt_length = 77;
	$words = explode(' ', $mycontent, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '');
		$mycontent = implode(' ', $words);
	endif;
	$mycontent = '<p>' . $mycontent . '... <a href="'.get_permalink($post->post_ID).'">read more &raquo;</a></p>';
    return $mycontent;
}
function admin_excerpt() {
	echo "<style>"; echo "textarea#excerpt {height:200px;}"; echo "</style>";
}
add_action('admin_head', 'admin_excerpt');
function is_type_page() { // Check if the current post is a page
	global $post;
	if ($post->post_type == 'page') {
		return true;
	} else {
		return false;
	}
}
function my_render_ie_pie() {
   echo '
   <!--[if lte IE 8]>
   <style type="text/css" media="screen">
       #ltf_banner span, #rgt_banner span, .ltf_content, .widget_area, .ltf_content h2, .widget_area h2, .inner_ltf_content, #inner_content, #inner_widget_area .inner_widget h2, #inner_content_wrapper h1 {
         behavior: url('.trailingslashit(get_bloginfo("template_url")).'PIE.htc);
      }
   </style>
   <![endif]-->';
   }
add_action('wp_head', 'my_render_ie_pie', 8);
if ( ! function_exists( 'mytheme_comment' ) ) :
function mytheme_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">

        <div class="reply_widget">
            <div class="reply_lftcolmn">
                <div class="reply_img">
                    <?php echo get_avatar( $comment, 30 ); ?>
                    <h3><?php printf( __( '%s' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?></h3>
                </div>
                <div class="time"><span><?php printf( __( '%1$s at %2$s' ), get_comment_date(),  get_comment_time() ); ?></span></div>
            </div>
            <div class="reply_rgtcolmn">
                <?php comment_text(); ?>
            </div>

			<?php if ( $comment->comment_approved == '0' ) : ?>
                <em><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
                <br />
            <?php endif; ?>

        </div>
	</div><!-- #comment-##  -->
	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)'), ' ' ); ?></p>

	<?php
			break;
	endswitch;
}
endif;
function dsrating_func($atts, $content = 2.5){
	$atts['url'] = "/457-visa-health-insurance/iman/";
	$atts['reviews'] = "1";
	$content = 2.5;
	if($atts['show'] == '') $atts['show'] = "iman";
	switch($atts['show']){
		case "iman":
			$atts['url'] = "/457-visa-health-insurance/iman/";
			$atts['reviews'] = "44";
			$content = 2.3;
			break;
		case "iman-couples":
			$atts['url'] = "/457-visa-health-insurance/couples/iman/";
			$atts['reviews'] = "27";
			$content = 2.6;
			break;
		case "iman-over":
			$atts['url'] = "/overseas-visitor-health-insurance/iman-insurance/";
			$atts['reviews'] = "29";
			$content = 2.6;
			break;
		case "iman-family":
			$atts['url'] = "/457-visa-health-insurance/family/iman/";
			$atts['reviews'] = "28";
			$content = 2.7;
			break;
		case "frank-over-couples":
			$atts['url'] = "/overseas-visitor-health-insurance/frank/";
			$atts['reviews'] = "35";
			$content = 2.6;
			break;
		case "iman-485":
			$atts['url'] = "/485-visa/health-insurance/iman-review/";
			$atts['reviews'] = "207";
			$content = 2.6;
			break;
		case "hif":
			$atts['url'] = "/457-visa-health-insurance/hif/";
			$atts['reviews'] = "60";
			$content = 3.3;
			break;
		case "hif-couples":
			$atts['url'] = "/457-visa-health-insurance/couples/hif/";
			$atts['reviews'] = "45";
			$content = 3.4;
			break;
		case "hif-family":
			$atts['url'] = "/457-visa-health-insurance/family/hif/";
			$atts['reviews'] = "53";
			$content = 3.3;
			break;
		case "hif-over":
			$atts['url'] = "/overseas-visitor-health-insurance/hif-review/";
			$atts['reviews'] = "44";
			$content = 3.3;
			break;
		case "hif-over-couples":
			$atts['url'] = "/overseas-visitor-health-insurance/couples/hif/";
			$atts['reviews'] = "44";
			$content = 3.3;
			break;
		case "hif-485":
			$atts['url'] = "/485-visa/health-insurance/hif-review/";
			$atts['reviews'] = "44";
			$content = 3.3;
			break;
		case "bupa":
			$atts['url'] = "/457-visa-health-insurance/bupa/";
			$atts['reviews'] = "296";
			$content = 1.4;
			break;
		case "bupa-over":
			$atts['url'] = "/overseas-visitor-health-insurance/bupa-review/";
			$atts['reviews'] = "162";
			$content = 1.3;
			break;
		case "bupa-485":
			$atts['url'] = "/485-visa/health-insurance/bupa-485-visa-insurance/";
			$atts['reviews'] = "162";
			$content = 1.3;
			break;
		case "australian-unity":
			$atts['url'] = "/457-visa-health-insurance/australian-unity/";
			$atts['reviews'] = "85";
			$content = 1.6;
			break;
		case "australian-unity-family":
			$atts['url'] = "/457-visa-health-insurance/family/australian-unity/";
			$atts['reviews'] = "71";
			$content = 1.7;
			break;
		case "australian-unity-485":
			$atts['url'] = "/485-visa/health-insurance/australian-unity/";
			$atts['reviews'] = "44";
			$content = 1.9;
			break;
		case "australian-unity-over":
			$atts['url'] = "/overseas-visitor-health-insurance/australian-unity-review/";
			$atts['reviews'] = "44";
			$content = 1.9;
			break;
		case "australian-unity-couples":
			$atts['url'] = "/457-visa-health-insurance/couples/australian-unity/";
			$atts['reviews'] = "55";
			$content = 1.8;
			break;
		case "australian-unity-over-couples":
			$atts['url'] = "/overseas-visitor-health-insurance/couples/australian-unity/";
			$atts['reviews'] = "57";
			$content = 1.8;
			break;
		case "medibank":
			$atts['url'] = "/457-visa-health-insurance/medibank/";
			$atts['reviews'] = "419";
			$content = 1.5;
			break;
		case "medibank-485":
			$atts['url'] = "/485-visa/health-insurance/medibank-review/";
			$atts['reviews'] = "195";
			$content = 1.8;
			break;
		case "medibank-over":
			$atts['url'] = "/overseas-visitor-health-insurance/medibank-review/";
			$atts['reviews'] = "195";
			$content = 1.8;
			break;
		case "medibank-over-couples":
			$atts['url'] = "/overseas-visitor-health-insurance/couples/medibank/";
			$atts['reviews'] = "201";
			$content = 1.8;
			break;
		case "hbf":
			$atts['url'] = "/457-visa-health-insurance/hbf/";
			$atts['reviews'] = "65";
			$content = 2.1;
			break;
		case "frank":
			$atts['url'] = "/457-visa-health-insurance/frank-review/";
			$atts['reviews'] = "238";
			$content = 4.5;
			break;
		case "frank-framily":
			$atts['url'] = "/457-visa-health-insurance/family/frank/";
			$atts['reviews'] = "202";
			$content = 4.6;
			break;
		case "frank-over":
			$atts['url'] = "/overseas-visitor-health-insurance/frank/";
			$atts['reviews'] = "121";
			$content = 4.8;
			break;
		case "anz":
			$atts['url'] = "/currency-exchange/anz/";
			$atts['reviews'] = "292";
			$content = 1.8;
			break;
		case "commbank":
			$atts['url'] = "/currency-exchange/commbank/";
			$atts['reviews'] = "336";
			$content = 2.4;
			break;
		case "nab":
			$atts['url'] = "/currency-exchange/nab/";
			$atts['reviews'] = "261";
			$content = 1.9;
			break;
		case "ozforex":
			$atts['url'] = "/currency-exchange/ozforex/";
			$atts['reviews'] = "128";
			$content = 4.1;
			break;
		case "westpac":
			$atts['url'] = "/currency-exchange/westpac/";
			$atts['reviews'] = "191";
			$content = 2.0;
			break;
		case "ti-nib":
			$atts['url'] = "/travel-insurance/nib/";
			$atts['reviews'] = "193";
			$content = 3.5;
			break;
		case "ti-iag":
			$atts['url'] = "/travel-insurance/insure-and-go/";
			$atts['reviews'] = "119";
			$content = 3.1;
			break;
		case "ti-cm":
			$atts['url'] = "/travel-insurance/cover-more/";
			$atts['reviews'] = "1221";
			$content = 4.0;
			break;
		case "ti-bw":
			$atts['url'] = "/travel-insurance/bankwest-travel-insurance/";
			$atts['reviews'] = "191";
			$content = 1.8;
			break;
		case "ti-wp":
			$atts['url'] = "/travel-insurance/westpac-travel-insurance/";
			$atts['reviews'] = "190";
			$content = 2.0;
			break;
	}
	$ret_str = '<a href="' . $atts['url'] . '">';
	if(!is_numeric($content)) $content = 2.5;
	if($content > 5) $content = 5;
	$content = number_format(round($content,2),2);
	$stars = explode(".",$content);
	$stars['on'] = round($stars[0]);
	if(!isset($stars[1])) $stars[1] = 0;
	$stars['half'] = 0;
	$stars['off'] = 0;
	if($stars[1] <= 25) $stars['off'] = 5 - $stars['on'];
	else if($stars[1] >= 75) {
		$stars['on']++;
		$stars['off'] = 5 - $stars['on'];
	}
	else {
		$stars['half'] = 1;
		$stars['off'] = 5 - ($stars['on'] + 1);
	}
	$red = "";
	if($content <= 2.7) $red = "_red";
	for($i=1;$i<=$stars['on'];$i++) $ret_str .= '<img alt="" src="/wp-content/uploads/2012/11/on_star' . $red . '.png" width="23" height="21" />';
	for($i=1;$i<=$stars['half'];$i++) $ret_str .= '<img alt="" src="/wp-content/uploads/2012/11/half_star' . $red . '.png" width="23" height="21" />';
	for($i=1;$i<=$stars['off'];$i++) $ret_str .= '<img alt="" src="/wp-content/uploads/2012/11/off_star.png" width="23" height="21" />';

	$ret_str .= '<br />' . $atts['reviews'] . ' Reviews</a>';
	return $ret_str;
}
function adspeed_func($atts){
	if($atts['type'] == '') $atts['type'] = "text";
	if($atts['show'] == '') $atts['show'] = "hif";
	if($atts['price'] == '') $atts['price'] = "NA";
	switch($atts['show']){
		case "hif":
			if($atts['type'] == "img"){
				if($atts['featured'] == "yes") $atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186514&wd=132&ht=41&pair=asemail" target="_blank"><img style="border:0px;" src="http://g.adspeed.net/ad.php?do=img&aid=186514&wd=132&ht=41&pair=asemail" width="132" height="41"/></a>';
				else $atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186422&wd=132&ht=41&pair=asemail" target="_blank"><img style="border:0px;" src="http://g.adspeed.net/ad.php?do=img&aid=186422&wd=132&ht=41&pair=asemail" width="132" height="41"/></a>';
			} else {
				$atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186416&t=1387434443&auth=963952e524858117e235d86fbd0bbaf5" target="_blank">' . $atts['price'] . '</a><div class="adspeed"><img src="http://g.adspeed.net/ad.php?do=imp&aid=186416&pair=asemail" alt="i" width="1" height="1"/>';
			}
			break;
		case "485hif":
			if($atts['type'] == "img"){
				if($atts['featured'] == "yes") $atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186514&wd=132&ht=41&pair=asemail" target="_blank"><img style="border:0px;" src="http://g.adspeed.net/ad.php?do=img&aid=186514&wd=132&ht=41&pair=asemail" width="132" height="41"/></a>';
				else $atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186784&wd=132&ht=41&pair=asemail" target="_blank"><img style="border:0px;" src="http://g.adspeed.net/ad.php?do=img&aid=186784&wd=132&ht=41&pair=asemail" width="132" height="41"/></a>';
			} else {
				$atts['url'] = '<a href="http://g.adspeed.net/ad.php?do=clk&aid=186783&t=1388134231&auth=8fd13981e788db121136cbe271eca84f" target="_blank">' . $atts['price'] . '</a><div class="adspeed"><img src="http://g.adspeed.net/ad.php?do=imp&aid=186783&pair=asemail" alt="i" width="1" height="1"/>';
			}
			break;
	}
	return $atts['url'];
}
add_shortcode( 'dsrating', 'dsrating_func');
add_shortcode( 'adspeed', 'adspeed_func');
include("tools/breadcrumbs.php");
include("tools/add_meta_box.php");
//include("tools/ratings-post-type.php");
include("tools/write-panel.php");
include("tools/testimonials-post-types.php");
//Remove Feeds
add_action( 'wp_head', 'wpse33072_wp_head', 1 );
/**
 * Remove feed links from wp_head
 */
function wpse33072_wp_head()
{
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
    remove_action('wp_head', 'rsd_link');
}
foreach( array( 'rdf', 'rss', 'rss2', 'atom' ) as $feed ){
    add_action( 'do_feed_' . $feed, 'wpse33072_remove_feeds', 1 );
}
unset( $feed );
/**
 * prefect actions from firing on feeds when the `do_feed` function is
 * called
 */
function wpse33072_remove_feeds(){
    // redirect the feeds! don't just kill them
    wp_redirect( home_url(), 301 );
    exit();
}
add_action( 'init', 'wpse33072_kill_feed_endpoint', 99 );
/**
 * Remove the `feed` endpoint
 */
function wpse33072_kill_feed_endpoint(){
remove_action('wp_head', 'shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
    // This is extremely brittle.
    // $wp_rewrite->feeds is public right now, but later versions of WP
    // might change that
    global $wp_rewrite;
    //$wp_rewrite->feeds = array();
}
register_activation_hook( __FILE__, 'wpse33072_activation' );
/**
 * Activation hook
 */
function wpse33072_activation(){
    wpse33072_kill_feed_endpoint();
    flush_rewrite_rules();
}
add_filter( 'emoji_svg_url', '__return_false' );

function show_social_media($url, $title, $class = "sharethis_home"){
	//<a href="https://www.facebook.com/sharer/sharer.php?u=' . urlencode($url) . '" target="_blank"><img src="http://www.457visacompared.com.au/wp-content/themes/457visa/images/facebook_counter.png" width="60" height="22" /></a>
	echo '<div class="' . $class . '">
<a href="#" onclick=\'window.open("https://www.facebook.com/sharer/sharer.php?u=' . urlencode($url) . '" ,"_blank", "toolbar=no, scrollbars=no, resizable=no, top=300, left=300, width=300, height=300")\'><img src="/wp-content/themes/457visa/images/facebook_counter.png" width="60" height="22"/></a>
<a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-url="' . $url . '" data-text="' . $title . '">Tweet</a>';
echo "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
echo '<div class="g-plusone" data-annotation="inline" data-width="300"></div>';
echo "<script type='text/javascript'>
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>";
//FB Facepiles
if(is_front_page()){
	echo '<br /><br /><br />
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2F457Visa&amp;width=600&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=379978402080275" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:600px; height:290px;" allowTransparency="true"></iframe>
';
} else{
	echo '<br /><br /><br />
<iframe src="//www.facebook.com/plugins/facepile.php?app_id&amp;href=https%3A%2F%2Fwww.facebook.com%2F457Visa&amp;action&amp;width=600&amp;height&amp;max_rows=1&amp;colorscheme=light&amp;size=medium&amp;show_count=true&amp;appId=379978402080275" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:600px;" allowTransparency="true"></iframe>
';
}
echo "</div>";
}
if ( ! is_admin() ) {
function defer_parsing_of_js ( $url ) {
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    //if ( strpos( $url, 'jquery.js' ) ) return $url;
    return "$url' async onload='myinit()";
}
//add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
function _remove_script_version( $src ){
	$parts = explode( '?', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
}

add_shortcode('dschart', 'dschart_func');
function dschart_func($atts, $content=""){
	$aurl = explode("/",$_SERVER["REQUEST_URI"]);
	if($aurl[count($aurl) - 2] == "amp") {
		if(!$atts['ampimage']) return "";
		else return '<img class="aligncenter"  src="' . $atts['ampimage'] . '" />';
	}
	$titles = array();
	$series = array();
	$i = 1;
	if(!$atts['height']) $atts['height'] = "200px";
	if(!$atts['width']) $atts['width'] = "100";
	$max = 0;
	do {
		if(!isset($atts['row' . $i])){
			break;
		}
		$data = explode("|",$atts['row' . $i]);
		$titles[] = '"' . $data[0] . '"';
		if(!$data[1]) $data[1] = 0;
		if(!$data[2]) $data[2] = "#0053C1";
		if(!$data[3]) $data[3] = "#47BBFF";
		if(!$data[4]) $data[4] = "#";
		if($data[1] > $max) $max = $data[1];
		$series[$i] = $data;
		$i++;
	} while ($i <= 10);
	$max = round($max);
	$max = $max - ($max % 10);
	$ret_str = '<div id="' . $atts['name'] . '" style="height: ' . $atts['height'] . '; width: ' . $atts['width'] . '%;"></div>
<script type="text/javascript">
var ' . $atts['name'] . '_flag = false;
$(window).scroll(function () {
	if(' . $atts['name'] . '_flag == false){
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $("#' . $atts['name'] . '").offset().top;
        var elementBottom = elementTop + $("#' . $atts['name'] . '").height();
    if ((pageTop < elementTop) && (pageBottom > elementBottom)) {
    		  ' . $atts['name'] . '_flag = true;
              $("#' . $atts['name'] . '").highcharts({
              chart: {
                   type: "bar",
                  options3d: {
                     	enabled: false,
                  	alpha: 1,
                	beta: 0,
                	depth: 40
            	  }
               },
               credits : {
                      enabled: false
               },
               title: {
                text: "' . $atts['title'] . '",
                useHTML:true
                },
                xAxis: {categories: [' . implode(',',$titles) . ']
                },
               plotOptions: {
                     bar: {
                    dataLabels: {
                    enabled: true,
                    inside:true,
                    align: "right",
                    color: "#FFFFFF",
                    x: -10
                }
            },
            series: {
                cursor: "pointer",
                pointPadding: 0.1,
                groupPadding: 0,
                point: {
                    events: {
                        click: function () {
                        	if(this.options.url == "#") return;
                            location.href = this.options.url;
                        }
                    }
                }
            }
        },
        legend: {enabled:false},
        yAxis: { 
          max: ' . $max . ',
          minTickInterval:0.001,
          title: {
             text: ""}},
        tooltip: { formatter: function() { return this.x + ": " + this.y;}},
        series: [{
           data: [';

		foreach($series as $d){
			$ret_str .= '{name:"' . $d[0] . '",
           	color:{linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },stops: [[0, "' . $d[2] . '"],[1, "' . $d[3] . '"]]},
			y:' . $d[1] . ',url:"' . $d[4] . '"},';
        }
		$ret_str .= ']
           }],
           });
    }}
});
</script>';
	return $ret_str;
}


add_shortcode('bbchart', 'bbchart_func');
function bbchart_func($atts, $content=""){
	$series = array();
	$i = 1;
	if(!$atts['height']) $atts['height'] = "200px";
	if(!$atts['width']) $atts['width'] = "100";

	do {
		if(!isset($atts['row' . $i])){
			break;
		}
		$series[$i] = "[" . $atts['row' . $i] . "]";
		$i++;
	} while ($i <= 10);


	$ret_str = '<div id="bbp' . $atts['name'] . '" style="clear:both; height: ' . $atts['height'] . '; width: ' . $atts['width'] . '%;"></div>
<script type="text/javascript">
$(document).ready(function(){
     
    var arr = [' . implode(",",$series) .  '];
     
    bbp_' . $atts['name'] . ' = $.jqplot("bbp' . $atts['name'] .  '",[arr],{
        title: "' . $atts['title'] . '",
        seriesDefaults:{
            renderer: $.jqplot.BubbleRenderer,
            rendererOptions: {
				bubbleGradients: true,
                bubbleAlpha: 0.6,
                highlightAlpha: 0.8,
                showLabels: true
            },
            shadow: true,
            shadowAlpha: 0.05
        },
		axes:{
			xaxis:{
				label:"' . $atts['xtitle'] . '",
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				labelOptions: {
					fontSize: "14pt"
				}
			},
			yaxis:{
				label:"' . $atts['ytitle'] . '",
				labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
				labelOptions: {
					fontSize: "14pt"
				}
			}
        }
    });
});
</script>';
	return $ret_str;

}remove_action( 'wp_head', 'wp_resource_hints', 2 );
?>
