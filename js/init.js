jQuery(document).ready(function(jQuery){
	
	var $wide = jQuery(window).width();
	
	if($wide > 1020) {
		$contHeight = jQuery('.ltf_content').height();
		$sideHeight = jQuery('.widget_area').height();
			
		if($contHeight > $sideHeight)
			$newHeight = $contHeight + 10;
		else
			$newHeight = $sideHeight + 10;
		
		jQuery(".ltf_content").css('minHeight', $newHeight);
		jQuery(".widget_area").css('minHeight', $newHeight);
	}

});