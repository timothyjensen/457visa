		jQuery.fn.qtip.styles.mystyle = { // Last part is the name of the style
		   width: 300,
		   padding: 10,
		   background: '#46B44A',
		   fontSize:12,
		   fontFamily:'Verdana',
		   color: '#fff',
		   textAlign: 'left',
		   
		  tip: { // Now an object instead of a string
			 corner: 'bottomMiddle', // We declare our corner within the object using the corner sub-option
			 color: '#46B44A',
			 size: {
				x: 20, // Be careful that the x and y values refer to coordinates on screen, not height or width.
				y : 8 // Depending on which corner your tooltip is at, x and y could mean either height or width!
			 }	 
		  },
		   border: {
			  width: 2,
			  radius: 1,
			  color: '#46B44A'
		   },
		   name: 'dark' // Inherit the rest of the attributes from the preset dark style
		}
		jQuery(document).ready(function() 
		{
			jQuery('.icon1').qtip({
			   	content: '<strong>OUT-PATIENT MEDICAL COVER</strong><br/>100% of MBS Fees (Fees for medical services as set by Medicare<br/>100% of the MBS Fee for out-patient doctor and specialist visits and services (including<br/>pathology and radiology) that are listed under the Medicare Benefits Schedule (MBS).<br/> Hospital Emergency Room Visits when not admitted to hospital but certified by the treating doctor as a genuine emergency<br/>Artificial Aids – ask IMAN for details of specific restrictions and replacements<br/>Exclusions:<br/>Psychology Out-patient services<br/>Psychiatric Out-patient services<br/>Antenatal and postnatal Out-patient services',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon2').qtip({
			   	content: 'Extras Covered - PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES<br/>Benefits apply after a 2 month Waiting Period has been served, and are subject to specified Annual<br/>Limits:<br>Single up to a maximum of jQueryA1000 per Membership Year in total<br/>Family/Couple up to a maximum of jQueryA2000 per Membership Year in total<br/>Treatment by a  registered physiotherapist, osteopath or chiropractor, due to Injury or Sickness whilst in Australia.',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon3').qtip({
			   	content: 'Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon4').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon5').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon6').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon7').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon8').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon9').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})
			jQuery('.icon10').qtip({
			   	content: '<strong>Extras Cover Includes  PHYSIOTHERAPY, OSTEOPATHY AND CHIROPRACTIC SERVICES and the items listed below:</strong>',
				style: 'mystyle',
				position: {
					  corner: {
						 target: 'topMiddle',
						 tooltip: 'bottomMiddle'
					  }
				   }
			})

		});