<?php
/**
 * Template Name: Checklist template
 *
 */ 
get_header(); ?>

<div id="inner_content_area"><!--start content_area-->
	<div id="inner-wrapper"><!--start inner-wrapper-->
        <div class="insurance-topcontent migration-agents"><!--start insurance-topcontent-->
            <div class="text-content">
            <div class="block_checklisttopcontent">
                <?php the_block('checklisttopcontent'); ?>                  
             </div>
        </div>
        <?php if(has_post_thumbnail()) { the_post_thumbnail( 'bank_thumbnail' ) ;} ?>
        <div class="clear"></div>
    </div><!--//end #insurance-topcontent-->
	</div><!--//end #content_area-->
	<div class="clear"></div>
    
	<div id="insurance-outerwrap">
        <div class="checklist_wrap">
            	<div class="checklist_topimg"><img src="<?php bloginfo('template_url') ?>/images/checklist_topicon.png" alt="" /></div>
            	<div class="checklist_bg"><div class="checklist_top"><div class="checklist_btm">
                	<div class="checklist_maincontent">
                    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>   
                    	<div class="checklist_content">
                        	<?php the_content() ?>
                        </div>
						<?php show_social_media(get_permalink(), get_the_title()); ?>
                        <?php endwhile; endif; ?>
                    </div>
                </div></div></div>
            </div>
	</div>

	
</div>
                  
                    
<?php get_footer(); ?>