<?php
/**
 * Template Name: Overseas Insurance template
 *
 */ 

$imgtitle = "overseas health insurance";
$imgstyle = "";
$insurance_wrap = "";

if(isset($_POST['catID']) && isset($_POST['catID2'])){
	switch($_POST['catID']){
		case 1:
			if($_POST['catID2'] == 1) $page_red_id = 2013;
			if($_POST['catID2'] == 2) $page_red_id = 2025;
			if($_POST['catID2'] == 3) $page_red_id = 2027;
			break;
		case 2:
			if($_POST['catID2'] == 1) $page_red_id = 2016;
			if($_POST['catID2'] == 2) $page_red_id = 2022;
			if($_POST['catID2'] == 3) $page_red_id = 2031;
			break;
		case 3:
			if($_POST['catID2'] == 1) $page_red_id = 2018;
			if($_POST['catID2'] == 2) $page_red_id = 2020;
			if($_POST['catID2'] == 3) $page_red_id = 2033;
			break;
	}
	header("Location: " . get_permalink($page_red_id));
	die('');
}


switch($post->ID){
	case 2013:
		$_GET['catID'] = 1; $_GET['catID2'] = 1; break;
	case 2025:
		$_GET['catID'] = 1; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; $imgtitle = "visitor health insurance"; break;
	case 2027:
		$_GET['catID'] = 1; $_GET['catID2'] = 3; $imgtitle = "overseas visitor health insurance"; break;
	case 2016:
		$_GET['catID'] = 2; $_GET['catID2'] = 1; break;
	case 2022:
		$_GET['catID'] = 2; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; $imgtitle = "visitor health insurance"; break;
	case 2031:
		$_GET['catID'] = 2; $_GET['catID2'] = 3; $imgtitle = "overseas visitor health insurance"; break;
	case 2018:
		$_GET['catID'] = 3; $_GET['catID2'] = 1; break;
	case 2020:
		$_GET['catID'] = 3; $_GET['catID2'] = 2; $insurance_wrap = "insurance-wrap-2"; $imgtitle = "visitor health insurance"; break;
	case 2033:
		$_GET['catID'] = 3; $_GET['catID2'] = 3; $imgtitle = "overseas visitor health insurance"; break;
}

get_header(); ?>

        <div id="inner_content_area"><!--start content_area-->
        	<div id="inner-wrapper"><!--start inner-wrapper-->
                <div class="insurance-topcontent"><!--start insurance-topcontent-->
                    <div class="text-content">
                    
            	<?php 
            			$imgtitle = trim(get_post_meta($post->ID, 'top_img_text', true));
            			echo trim(get_post_meta($post->ID, 'custom_title', true));
            			$bg_img_url = trim(get_post_meta($post->ID, 'bg_img_url', true)); 
            			if($bg_img_url != '') {
            				$bg_img_url = "background: url('" . $bg_img_url . "') no-repeat scroll 0% 0% transparent!important";
            				$bg_img_url = 'style="' . $bg_img_url . '"';
            			}
            	?>
               
                    

                    <div class="selec-field">
                    	<form name="catSelect" action="" method="post" >
                    	<div class="rowElem rowElem1">
                            <label>I'm looking for cover:</label>
                            <select name="catID" id="catID" onchange="catSelect.submit()">
                                <option value="1"<?php if($_GET['catID'] == '1') echo ' selected="selected"' ?>>Just Myself</option>
                            
                                <option value="2"<?php if($_GET['catID'] == '2') echo ' selected="selected"' ?>>Me &amp; my partner</option>
                                <option value="3"<?php if($_GET['catID'] == '3') echo ' selected="selected"' ?>>My family</option>
                            </select>
                        </div>
                        <div class="rowElem">
                        <label>My Main Priority is:</label>
                            <select name="catID2" id="catID2" onchange="catSelect.submit()">
                                <option value="1"<?php if($_GET['catID2'] == '1') echo ' selected="selected"' ?>>To Meet Health Cover Requirements</option>
                            
                                <option value="2"<?php if($_GET['catID2'] == '2') echo ' selected="selected"' ?>>Health Cover With Some Extras</option>
                            
                                <option value="3"<?php if($_GET['catID2'] == '3') echo ' selected="selected"' ?>>To Have Top Level Cover</option>
                                
                             </select>
                         </div>
                       </form>
                    </div>
 
                </div>

                <img src="<?php echo trim(get_post_meta($post->ID, 'top_img_url', true)); ?>" width="300" height="230"  alt="<?php echo $imgtitle; ?>" title="<?php echo $imgtitle; ?>" />
                <div class="clear"></div>
            </div><!--//end #insurance-topcontent-->
        </div><!--//end #content_area-->
        <div class="clear"></div>
        
         <div id="insurance-outerwrap">
            <div id="insurance-wrap" class="<?php echo $insurance_wrap; ?>" <?php echo $bg_img_url; ?>>
                   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   <div class="insurancebox_content">
                   <?php the_content('More...'); ?>
                   </div>
                   <?php endwhile;endif;
                   wp_reset_query();
                   ?>
            </div>
            <div class="instructions">
            	<div class="instructions_inner">
            	<h4>This information is indicative and provided for general information only.<br />Please refer to the relevant policy brochure for full details including waiting
                 period.</h4>
                 <div class="instructions_one">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_tick.png" align="tick" /><span>This policy is a good match for this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_two">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_exclo.png" align="tick" /><span>This policy only covers some areas of this health insurance item</span>
                 </div>
                 <div class="instructions_one instruction_three">
                 	<img src="<?php bloginfo('template_url') ?>/images/ico_cross.png" align="tick" /><span>This policy does not offer this medical cover element</span>
                 </div>
                 </div>
            </div>
            <div class="clear"></div>
                    
            <div class="insurance-btmcontent">
            		<div class="bankbox_bg"><div class="bankbox_topbg"><div class="bankbox_btmbg">
                        <div class="bank-entry">
                            <div class="block_ratingsbtmcontent">
                    			<?php the_block('ratingsbtmcontent'); ?>                     
                			</div>
                			<?php /*
                            <div class="sharethis"> <span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='facebook'></span><span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='twitter'></span><span class='st_sharethis_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='sharethis'></span><span class='st_plusone_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='plusone'></span></div> 
                            */ 
                            show_social_media(get_permalink(), get_the_title()); 
                            ?>
                        </div>
                    </div></div></div>
                </div>
       </div>
       </div>
                  
                    
<?php get_footer(); ?>