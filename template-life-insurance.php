<?php
/**
 * Template Name: Life Insurance template
 *
 */ 


get_header(); ?>

        <div id="inner_content_area"><!--start content_area-->
        	<div id="inner-wrapper"><!--start inner-wrapper-->
                <div class="insurance-topcontent"><!--start insurance-topcontent-->
                    <div class="text-content">
            			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Life Insurance Table Widget') );?>
                	</div>
                <img src="/wp-content/themes/457visa/images/life-insurance-cost.png" width="150" style="width:150px; height:149px; margin-top: 10px" alt="Life Insurance" title="Life Insurance" />
                <div class="clear"></div>
            </div><!--//end #insurance-topcontent-->
        </div><!--//end #content_area-->
        <div class="clear"></div>
        
         <div id="insurance-outerwrap">
                    
            <div class="insurance-btmcontent">
            		<div class="bankbox_bg"><div class="bankbox_topbg"><div class="bankbox_btmbg">
                        <div class="bank-entry">
                            <div class="block_ratingsbtmcontent">               
             
                   			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   				<?php the_content('More...'); ?>
                   			<?php endwhile;endif;
                   				wp_reset_query();
                   			?>
               
                			</div>
							<?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
                        </div>
                    </div></div></div>
                </div>
       </div>
       </div>
                  
                    
<?php get_footer(); ?>