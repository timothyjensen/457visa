<?php
/**
 * Template Name: Internet template
 *
 */ 
get_header(); ?>

<div id="inner_content_area"><!--start content_area-->
	<div id="inner-wrapper"><!--start inner-wrapper-->
        <div class="insurance-topcontent"><!--start insurance-topcontent-->
            <div class="text-content">
            <div class="block_internettopcontent">
                <?php the_block('internettopcontent'); ?>                  
             </div>
        </div>
        <?php if(has_post_thumbnail()) { the_post_thumbnail( 'bank_thumbnail' ) ;} ?>
        <div class="clear"></div>
    </div><!--//end #insurance-topcontent-->
	</div><!--//end #content_area-->
	<div class="clear"></div>
    
	<div id="insurance-outerwrap">
    
        <div id="internet-wrap">
            <div class="insurancebox_content">
                <div class="block_internetratings">
                    <?php the_block('internetratings'); ?>                     
                </div>
             </div>
        </div>
        
        <div class="bank-btmcontent">
        <div class="bankbox_bg"><div class="bankbox_topbg"><div class="bankbox_btmbg">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="bank-entry">
                <?php the_content('More...'); ?>
            </div>
			<?php show_social_media(get_permalink(), get_the_title(), "sharethis"); ?>
            <?php endwhile;endif; ?>
        </div></div></div>
    	</div>
        
        
	</div>

	
</div>
                  
                    
<?php get_footer(); ?>